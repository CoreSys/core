<?php

$params = array(
    'emails'   => array(),
    'names'    => array(),
    'subjects' => array()
);

for ( $i = 0; $i <= 5; $i++ ) {
    $params[ 'emails' ][]   = 'email' . $i . '@domain.com';
    $params[ 'names' ][]    = 'Someone' . $i;
    $params[ 'subjects' ][] = 'Subject Title ' . $i;
}

echo preg_replace( '/\[\d+\]/', '[]', urldecode( http_build_query( $params ) ) );
