<?php

$sets = array(
    'lander'     => array( 'key' => 'landers', 'many' => TRUE ),
    'population' => array( 'key' => 'populations', 'many' => FALSE ),
    'source'     => array( 'key' => 'sources', 'many' => TRUE )
);

$output = array();

foreach ( $sets as $key => $data ) {
    $out = array();
    if ( $data[ 'many' ] ) {
        $out[] = 'public function add' . ucwords( $key ) . '(' . ucwords( $key ) . ' $' . $key . ', $rev = true) {';
    } else {
        $out[] = 'public function add' . ucwords( $key ) . '(' . ucwords( $key ) . ' $' . $key . ') {';
    }

    $out[] = 'if( !$this->' . $data[ 'key' ] . '->contains( $' . $key . ') ) {';
    $out[] = '$this->' . $data[ 'key' ] . '->add( $' . $key . ' );';
    if ( $data[ 'many' ] ) {
        $out[] = 'if( $rev ) {';
        $out[] = '$' . $key . '->addAccount( $this, false );';
        $out[] = '}';
    } else {
        $out[] = '$' . $key . '->setAccount( $this );';
    }
    $out[] = '}';
    $out[] = 'return $this;';

    $out[] = '}';

    if ( $data[ 'many' ] ) {
        $out[] = 'public function remove' . ucwords( $key ) . '(' . ucwords( $key ) . ' $' . $key . ', $rev = true) {';
    } else {
        $out[] = 'public function remove' . ucwords( $key ) . '(' . ucwords( $key ) . ' $' . $key . ') {';
    }

    $out[] = 'if( $this->' . $data[ 'key' ] . '->contains( $' . $key . ') ) {';
    $out[] = '$this->' . $data[ 'key' ] . '->removeElement( $' . $key . ' );';
    if ( $data[ 'many' ] ) {
        $out[] = 'if( $rev ) {';
        $out[] = '$' . $key . '->removeAccount( $this, false );';
        $out[] = '}';
    } else {
        $out[] = '$' . $key . '->setAccount( null );';
    }
    $out[] = '}';
    $out[] = 'return $this;';

    $out[] = '}';

    $output[] = implode( '<br>', $out );
}

echo implode( '<br><br>', $output );