(function() {
    /**
     * @return {?}
     */
    function encode() {
        var map = getParams();
        var letter;
        for (letter in map) {
            var value = map[letter];
            var data;
            data = value;
            if (6 !== data.length) {
                /** @type {boolean} */
                data = false;
            } else {
                var length;
                /** @type {boolean} */
                length = data.match(/^[a-z0-9]+$/) ? true : false;
                if (length) {
                    data = data.split("");
                    /** @type {number} */
                    index = length = 0;
                    for (;index < data.length;++index) {
                        length += data[index].charCodeAt(0);
                    }
                    /** @type {boolean} */
                    data = 465 !== length ? false : true;
                } else {
                    /** @type {boolean} */
                    data = false;
                }
            }
            if (data && ("undefined" === typeof disable_override || !disable_override)) {
                return value;
            }
        }
        return "e5c98c";
    }
    /**
     * @return {?}
     */
    function getParams() {
        var match;
        /** @type {RegExp} */
        var cx = /\+/g;
        /** @type {RegExp} */
        var rquickExpr = /([^&=]+)=?([^&]*)/g;
        /**
         * @param {string} text
         * @return {?}
         */
        var decode = function(text) {
            text = text.replace(cx, " ");
            var href;
            a: {
                /** @type {string} */
                href = text;
                /** @type {Array.<string>} */
                var codeSegments = "%20 %21 %22 %23 %24 %25 %26 %27 %28 %29 %2A %2B %2C %2D %2E %2F %3A %3B %3C %3D %3E %3F %40 %5B %5C %5D %5E %5F %7B %7C %7D %7E %60".split(" ");
                /** @type {number} */
                i = 0;
                for (;i < codeSegments.length;i++) {
                    if (-1 !== href.indexOf(codeSegments[i])) {
                        /** @type {boolean} */
                        href = true;
                        break a;
                    }
                }
                /** @type {boolean} */
                href = false;
            }
            return href ? decodeURIComponent(text) : text;
        };
        /** @type {string} */
        var selector = window.location.search.substring(1);
        var params = {};
        for (;match = rquickExpr.exec(selector);) {
            params[decode(match[1])] = decode(match[2]);
        }
        return params;
    }
    /**
     * @return {?}
     */
    function findCordovaPath() {
        /** @type {NodeList} */
        var codeSegments = document.getElementsByTagName("script");
        /** @type {number} */
        var i = 0;
        for (;i < codeSegments.length;i++) {
            if (-1 != codeSegments[i].src.indexOf("e5c98c")) {
                return codeSegments[i];
            }
        }
    }
    /**
     * @param {number} opt_attributes
     * @return {?}
     */
    function asciiToString(opt_attributes) {
        return String.fromCharCode.apply(null, arguments);
    }
    var self = {
        alphabet : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        lookup : null,
        ie : /MSIE /.test(navigator.userAgent),
        ieo : /MSIE [67]/.test(navigator.userAgent),
        /**
         * @param {(Array|string)} bytes
         * @return {?}
         */
        encode : function(bytes) {
            bytes = self.toUtf8(bytes);
            /** @type {number} */
            var i = -1;
            var n = bytes.length;
            var byte3;
            var chr2;
            /** @type {Array} */
            var pos = [];
            if (self.ie) {
                /** @type {Array} */
                var chunks = [];
                for (;++i < n;) {
                    byte3 = bytes[i];
                    chr2 = bytes[++i];
                    /** @type {number} */
                    pos[0] = byte3 >> 2;
                    /** @type {number} */
                    pos[1] = (byte3 & 3) << 4 | chr2 >> 4;
                    if (isNaN(chr2)) {
                        /** @type {number} */
                        pos[2] = pos[3] = 64;
                    } else {
                        byte3 = bytes[++i];
                        /** @type {number} */
                        pos[2] = (chr2 & 15) << 2 | byte3 >> 6;
                        /** @type {number} */
                        pos[3] = isNaN(byte3) ? 64 : byte3 & 63;
                    }
                    chunks.push(self.alphabet.charAt(pos[0]), self.alphabet.charAt(pos[1]), self.alphabet.charAt(pos[2]), self.alphabet.charAt(pos[3]));
                }
                return chunks.join("");
            }
            /** @type {string} */
            chunks = "";
            for (;++i < n;) {
                byte3 = bytes[i];
                chr2 = bytes[++i];
                /** @type {number} */
                pos[0] = byte3 >> 2;
                /** @type {number} */
                pos[1] = (byte3 & 3) << 4 | chr2 >> 4;
                if (isNaN(chr2)) {
                    /** @type {number} */
                    pos[2] = pos[3] = 64;
                } else {
                    byte3 = bytes[++i];
                    /** @type {number} */
                    pos[2] = (chr2 & 15) << 2 | byte3 >> 6;
                    /** @type {number} */
                    pos[3] = isNaN(byte3) ? 64 : byte3 & 63;
                }
                chunks += self.alphabet[pos[0]] + self.alphabet[pos[1]] + self.alphabet[pos[2]] + self.alphabet[pos[3]];
            }
            return chunks;
        },
        /**
         * @param {Array} input
         * @return {?}
         */
        decode : function(input) {
            if (input.length % 4) {
                throw Error("decode failed.");
            }
            input = self.fromUtf8(input);
            /** @type {number} */
            var i = 0;
            var il = input.length;
            if (self.ieo) {
                /** @type {Array} */
                var output = [];
                for (;i < il;) {
                    if (128 > input[i]) {
                        output.push(String.fromCharCode(input[i++]));
                    } else {
                        if (191 < input[i] && 224 > input[i]) {
                            output.push(String.fromCharCode((input[i++] & 31) << 6 | input[i++] & 63));
                        } else {
                            output.push(String.fromCharCode((input[i++] & 15) << 12 | (input[i++] & 63) << 6 | input[i++] & 63));
                        }
                    }
                }
                return output.join("");
            }
            /** @type {string} */
            output = "";
            for (;i < il;) {
                /** @type {string} */
                output = 128 > input[i] ? output + String.fromCharCode(input[i++]) : 191 < input[i] && 224 > input[i] ? output + String.fromCharCode((input[i++] & 31) << 6 | input[i++] & 63) : output + String.fromCharCode((input[i++] & 15) << 12 | (input[i++] & 63) << 6 | input[i++] & 63);
            }
            return output;
        },
        /**
         * @param {string} number
         * @return {?}
         */
        toUtf8 : function(number) {
            /** @type {number} */
            var i = -1;
            var n = number.length;
            var cur;
            /** @type {Array} */
            var eventPath = [];
            if (/^[\x00-\x7f]*$/.test(number)) {
                for (;++i < n;) {
                    eventPath.push(number.charCodeAt(i));
                }
            } else {
                for (;++i < n;) {
                    cur = number.charCodeAt(i);
                    if (128 > cur) {
                        eventPath.push(cur);
                    } else {
                        if (2048 > cur) {
                            eventPath.push(cur >> 6 | 192, cur & 63 | 128);
                        } else {
                            eventPath.push(cur >> 12 | 224, cur >> 6 & 63 | 128, cur & 63 | 128);
                        }
                    }
                }
            }
            return eventPath;
        },
        /**
         * @param {?} input
         * @return {?}
         */
        fromUtf8 : function(input) {
            /** @type {number} */
            var index = -1;
            var length;
            /** @type {Array} */
            var eventPath = [];
            /** @type {Array} */
            var d = [];
            if (!self.lookup) {
                /** @type {number} */
                length = self.alphabet.length;
                self.lookup = {};
                for (;++index < length;) {
                    /** @type {number} */
                    self.lookup[self.alphabet.charAt(index)] = index;
                }
                /** @type {number} */
                index = -1;
            }
            length = input.length;
            for (;++index < length;) {
                d[0] = self.lookup[input.charAt(index)];
                d[1] = self.lookup[input.charAt(++index)];
                eventPath.push(d[0] << 2 | d[1] >> 4);
                d[2] = self.lookup[input.charAt(++index)];
                if (64 == d[2]) {
                    break;
                }
                eventPath.push((d[1] & 15) << 4 | d[2] >> 2);
                d[3] = self.lookup[input.charAt(++index)];
                if (64 == d[3]) {
                    break;
                }
                eventPath.push((d[2] & 3) << 6 | d[3]);
            }
            return eventPath;
        }
    };
    (function() {
        /**
         * @param {Object} context
         * @param {JSONType} exports
         * @return {?}
         */
        function stringify(context, exports) {
            /**
             * @param {string} name
             * @return {?}
             */
            function has(name) {
                if (has[name] !== undef) {
                    return has[name];
                }
                var stringify;
                if ("bug-string-char-index" == name) {
                    /** @type {boolean} */
                    stringify = false;
                } else {
                    if ("json" == name) {
                        stringify = has("json-stringify") && has("json-parse");
                    } else {
                        var value;
                        if ("json-stringify" == name) {
                            stringify = exports.stringify;
                            var stringifySupported = "function" == typeof stringify && isExtended;
                            if (stringifySupported) {
                                /** @type {function (): ?} */
                                (value = function() {
                                    return 1;
                                }).toJSON = value;
                                try {
                                    /** @type {boolean} */
                                    stringifySupported = "0" === stringify(0) && ("0" === stringify(new Number) && ('""' == stringify(new String) && (stringify(getClass) === undef && (stringify(undef) === undef && (stringify() === undef && ("1" === stringify(value) && ("[1]" == stringify([value]) && ("[null]" == stringify([undef]) && ("null" == stringify(null) && ("[null,null,null]" == stringify([undef, getClass, null]) && ('{"a":[1,true,false,null,"\\u0000\\b\\n\\f\\r\\t"]}' == stringify({
                                            a : [value, true, false, null, "\x00\b\n\f\r\t"]
                                        }) && ("1" === stringify(null, value) && ("[\n 1,\n 2\n]" == stringify([1, 2], null, 1) && ('"-271821-04-20T00:00:00.000Z"' == stringify(new RegExp(-864E13)) && ('"+275760-09-13T00:00:00.000Z"' == stringify(new RegExp(864E13)) && ('"-000001-01-01T00:00:00.000Z"' == stringify(new RegExp(-621987552E5)) && '"1969-12-31T23:59:59.999Z"' == stringify(new RegExp(-1))))))))))))))))));
                                } catch (d) {
                                    /** @type {boolean} */
                                    stringifySupported = false;
                                }
                            }
                            stringify = stringifySupported;
                        }
                        if ("json-parse" == name) {
                            stringify = exports.parse;
                            if ("function" == typeof stringify) {
                                try {
                                    if (0 === stringify("0") && !stringify(false)) {
                                        value = stringify('{"a":[1,true,false,null,"\\u0000\\b\\n\\f\\r\\t"]}');
                                        /** @type {boolean} */
                                        var returnFalse = 5 == value.a.length && 1 === value.a[0];
                                        if (returnFalse) {
                                            try {
                                                /** @type {boolean} */
                                                returnFalse = !stringify('"\t"');
                                            } catch (d) {
                                            }
                                            if (returnFalse) {
                                                try {
                                                    /** @type {boolean} */
                                                    returnFalse = 1 !== stringify("01");
                                                } catch (d) {
                                                }
                                            }
                                            if (returnFalse) {
                                                try {
                                                    /** @type {boolean} */
                                                    returnFalse = 1 !== stringify("1.");
                                                } catch (d) {
                                                }
                                            }
                                        }
                                    }
                                } catch (d) {
                                    /** @type {boolean} */
                                    returnFalse = false;
                                }
                            }
                            /** @type {(boolean|undefined)} */
                            stringify = returnFalse;
                        }
                    }
                }
                return has[name] = !!stringify;
            }
            if (!context) {
                context = Y.Object();
            }
            if (!exports) {
                exports = Y.Object();
            }
            var Number = context.Number || Y.Number;
            var String = context.String || Y.String;
            var tmp = context.Object || Y.Object;
            var RegExp = context.Date || Y.Date;
            var aborter = context.SyntaxError || Y.SyntaxError;
            var eURI = context.TypeError || Y.TypeError;
            var math = context.Math || Y.Math;
            var options = context.JSON || Y.JSON;
            if ("object" == typeof options) {
                if (options) {
                    exports.stringify = options.stringify;
                    exports.parse = options.parse;
                }
            }
            tmp = tmp.prototype;
            var getClass = tmp.toString;
            var isProperty;
            var forEach;
            var undef;
            var isExtended = new RegExp(-0xc782b5b800cec);
            try {
                /** @type {boolean} */
                isExtended = -109252 == isExtended.getUTCFullYear() && (0 === isExtended.getUTCMonth() && (1 === isExtended.getUTCDate() && (10 == isExtended.getUTCHours() && (37 == isExtended.getUTCMinutes() && (6 == isExtended.getUTCSeconds() && 708 == isExtended.getUTCMilliseconds())))));
            } catch (g) {
            }
            if (!has("json")) {
                var charIndexBuggy = has("bug-string-char-index");
                if (!isExtended) {
                    var floor = math.floor;
                    /** @type {Array} */
                    var months = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
                    /**
                     * @param {number} year
                     * @param {number} month
                     * @return {?}
                     */
                    var getDay = function(year, month) {
                        return months[month] + 365 * (year - 1970) + floor((year - 1969 + (month = +(1 < month))) / 4) - floor((year - 1901 + month) / 100) + floor((year - 1601 + month) / 400);
                    };
                }
                if (!(isProperty = tmp.hasOwnProperty)) {
                    /**
                     * @param {?} property
                     * @return {?}
                     */
                    isProperty = function(property) {
                        var members = {};
                        var constructor;
                        if ((members.__proto__ = null, members.__proto__ = {
                                toString : 1
                            }, members).toString != getClass) {
                            /**
                             * @param {boolean} property
                             * @return {?}
                             */
                            isProperty = function(property) {
                                var original = this.__proto__;
                                /** @type {boolean} */
                                property = property in (this.__proto__ = null, this);
                                this.__proto__ = original;
                                return property;
                            };
                        } else {
                            /** @type {(Function|null)} */
                            constructor = members.constructor;
                            /**
                             * @param {?} property
                             * @return {?}
                             */
                            isProperty = function(property) {
                                var parent = (this.constructor || constructor).prototype;
                                return property in this && !(property in parent && this[property] === parent[property]);
                            };
                        }
                        /** @type {null} */
                        members = null;
                        return isProperty.call(this, property);
                    };
                }
                /**
                 * @param {(Error|string)} elems
                 * @param {Function} callback
                 * @return {?}
                 */
                forEach = function(elems, callback) {
                    /** @type {number} */
                    var c = 0;
                    var Properties;
                    var members;
                    var property;
                    /** @type {number} */
                    (Properties = function() {
                        /** @type {number} */
                        this.valueOf = 0;
                    }).prototype.valueOf = 0;
                    members = new Properties;
                    for (property in members) {
                        if (isProperty.call(members, property)) {
                            c++;
                        }
                    }
                    /** @type {null} */
                    Properties = members = null;
                    if (c) {
                        /** @type {function (?, Function): undefined} */
                        forEach = 2 == c ? function(object, callback) {
                            var members = {};
                            /** @type {boolean} */
                            var prototype = "[object Function]" == getClass.call(object);
                            var property;
                            for (property in object) {
                                if (!(prototype && "prototype" == property)) {
                                    if (!isProperty.call(members, property)) {
                                        if (!!(members[property] = 1)) {
                                            if (!!isProperty.call(object, property)) {
                                                callback(property);
                                            }
                                        }
                                    }
                                }
                            }
                        } : function(object, callback) {
                            /** @type {boolean} */
                            var prototype = "[object Function]" == getClass.call(object);
                            var property;
                            var isConstructor;
                            for (property in object) {
                                if (!(prototype && "prototype" == property)) {
                                    if (!!isProperty.call(object, property)) {
                                        if (!(isConstructor = "constructor" === property)) {
                                            callback(property);
                                        }
                                    }
                                }
                            }
                            if (isConstructor || isProperty.call(object, property = "constructor")) {
                                callback(property);
                            }
                        };
                    } else {
                        /** @type {Array.<string>} */
                        members = "valueOf toString toLocaleString propertyIsEnumerable isPrototypeOf hasOwnProperty constructor".split(" ");
                        /**
                         * @param {?} object
                         * @param {Function} callback
                         * @return {undefined}
                         */
                        forEach = function(object, callback) {
                            /** @type {boolean} */
                            var length = "[object Function]" == getClass.call(object);
                            var property;
                            var _hasOwnProperty = !length && ("function" != typeof object.constructor && (objectTypes[typeof object.hasOwnProperty] && object.hasOwnProperty)) || isProperty;
                            for (property in object) {
                                if (!(length && "prototype" == property)) {
                                    if (!!_hasOwnProperty.call(object, property)) {
                                        callback(property);
                                    }
                                }
                            }
                            length = members.length;
                            for (;property = members[--length];_hasOwnProperty.call(object, property) && callback(property)) {
                            }
                        };
                    }
                    return forEach(elems, callback);
                };
                if (!has("json-stringify")) {
                    var data = {
                        92 : "\\\\",
                        34 : '\\"',
                        8 : "\\b",
                        12 : "\\f",
                        10 : "\\n",
                        13 : "\\r",
                        9 : "\\t"
                    };
                    /**
                     * @param {number} opt_attributes
                     * @param {number} value
                     * @return {?}
                     */
                    var toPaddedString = function(opt_attributes, value) {
                        return("000000" + (value || 0)).slice(-opt_attributes);
                    };
                    /**
                     * @param {string} value
                     * @return {?}
                     */
                    var quote = function(value) {
                        /** @type {string} */
                        var c = '"';
                        /** @type {number} */
                        var position = 0;
                        var len = value.length;
                        /** @type {boolean} */
                        var haveByte2 = !charIndexBuggy || 10 < len;
                        var input = haveByte2 && (charIndexBuggy ? value.split("") : value);
                        for (;position < len;position++) {
                            var i = value.charCodeAt(position);
                            switch(i) {
                                case 8:
                                    ;
                                case 9:
                                    ;
                                case 10:
                                    ;
                                case 12:
                                    ;
                                case 13:
                                    ;
                                case 34:
                                    ;
                                case 92:
                                    c += data[i];
                                    break;
                                default:
                                    c = 32 > i ? c + ("\\u00" + toPaddedString(2, i.toString(16))) : c + (haveByte2 ? input[position] : value.charAt(position));
                            }
                        }
                        return c + '"';
                    };
                    /**
                     * @param {number} i
                     * @param {string} self
                     * @param {Function} callback
                     * @param {number} properties
                     * @param {boolean} whitespace
                     * @param {string} indentation
                     * @param {Array} stack
                     * @return {?}
                     */
                    var serialize = function(i, self, callback, properties, whitespace, indentation, stack) {
                        var value;
                        var element;
                        var month;
                        var date;
                        var minutes;
                        var hours;
                        var udataCur;
                        var pdataOld;
                        var results;
                        try {
                            value = self[i];
                        } catch (x) {
                        }
                        if ("object" == typeof value && value) {
                            if (element = getClass.call(value), "[object Date]" != element || isProperty.call(value, "toJSON")) {
                                if ("function" == typeof value.toJSON) {
                                    if ("[object Number]" != element && ("[object String]" != element && "[object Array]" != element) || isProperty.call(value, "toJSON")) {
                                        value = value.toJSON(i);
                                    }
                                }
                            } else {
                                if (value > -1 / 0 && value < 1 / 0) {
                                    if (getDay) {
                                        date = floor(value / 864E5);
                                        /** @type {number} */
                                        element = floor(date / 365.2425) + 1970 - 1;
                                        for (;getDay(element + 1, 0) <= date;element++) {
                                        }
                                        month = floor((date - getDay(element, 0)) / 30.42);
                                        for (;getDay(element, month + 1) <= date;month++) {
                                        }
                                        /** @type {number} */
                                        date = 1 + date - getDay(element, month);
                                        /** @type {number} */
                                        minutes = (value % 864E5 + 864E5) % 864E5;
                                        /** @type {number} */
                                        hours = floor(minutes / 36E5) % 24;
                                        /** @type {number} */
                                        udataCur = floor(minutes / 6E4) % 60;
                                        /** @type {number} */
                                        pdataOld = floor(minutes / 1E3) % 60;
                                        minutes %= 1E3;
                                    } else {
                                        element = value.getUTCFullYear();
                                        month = value.getUTCMonth();
                                        date = value.getUTCDate();
                                        hours = value.getUTCHours();
                                        udataCur = value.getUTCMinutes();
                                        pdataOld = value.getUTCSeconds();
                                        minutes = value.getUTCMilliseconds();
                                    }
                                    /** @type {string} */
                                    value = (0 >= element || 1E4 <= element ? (0 > element ? "-" : "+") + toPaddedString(6, 0 > element ? -element : element) : toPaddedString(4, element)) + "-" + toPaddedString(2, month + 1) + "-" + toPaddedString(2, date) + "T" + toPaddedString(2, hours) + ":" + toPaddedString(2, udataCur) + ":" + toPaddedString(2, pdataOld) + "." + toPaddedString(3, minutes) + "Z";
                                } else {
                                    /** @type {null} */
                                    value = null;
                                }
                            }
                        }
                        if (callback) {
                            value = callback.call(self, i, value);
                        }
                        if (null === value) {
                            return "null";
                        }
                        element = getClass.call(value);
                        if ("[object Boolean]" == element) {
                            return "" + value;
                        }
                        if ("[object Number]" == element) {
                            return value > -1 / 0 && value < 1 / 0 ? "" + value : "null";
                        }
                        if ("[object String]" == element) {
                            return quote("" + value);
                        }
                        if ("object" == typeof value) {
                            i = stack.length;
                            for (;i--;) {
                                if (stack[i] === value) {
                                    throw eURI();
                                }
                            }
                            stack.push(value);
                            /** @type {Array} */
                            results = [];
                            /** @type {string} */
                            self = indentation;
                            indentation += whitespace;
                            if ("[object Array]" == element) {
                                /** @type {number} */
                                month = 0;
                                i = value.length;
                                for (;month < i;month++) {
                                    element = serialize(month, value, callback, properties, whitespace, indentation, stack);
                                    results.push(element === undef ? "null" : element);
                                }
                                /** @type {string} */
                                i = results.length ? whitespace ? "[\n" + indentation + results.join(",\n" + indentation) + "\n" + self + "]" : "[" + results.join(",") + "]" : "[]";
                            } else {
                                forEach(properties || value, function(i) {
                                    var element = serialize(i, value, callback, properties, whitespace, indentation, stack);
                                    if (element !== undef) {
                                        results.push(quote(i) + ":" + (whitespace ? " " : "") + element);
                                    }
                                });
                                /** @type {string} */
                                i = results.length ? whitespace ? "{\n" + indentation + results.join(",\n" + indentation) + "\n" + self + "}" : "{" + results.join(",") + "}" : "{}";
                            }
                            stack.pop();
                            return i;
                        }
                    };
                    /**
                     * @param {?} source
                     * @param {(Function|string)} object
                     * @param {string} width
                     * @return {?}
                     */
                    exports.stringify = function(source, object, width) {
                        var whitespace;
                        var restoreScript;
                        var properties;
                        var match;
                        if (objectTypes[typeof object] && object) {
                            if ("[object Function]" == (match = getClass.call(object))) {
                                /** @type {(Function|string)} */
                                restoreScript = object;
                            } else {
                                if ("[object Array]" == match) {
                                    properties = {};
                                    /** @type {number} */
                                    var i = 0;
                                    var length = object.length;
                                    var value;
                                    for (;i < length;value = object[i++], (match = getClass.call(value), "[object String]" == match || "[object Number]" == match) && (properties[value] = 1)) {
                                    }
                                }
                            }
                        }
                        if (width) {
                            if ("[object Number]" == (match = getClass.call(width))) {
                                if (0 < (width -= width % 1)) {
                                    /** @type {string} */
                                    whitespace = "";
                                    if (10 < width) {
                                        /** @type {number} */
                                        width = 10;
                                    }
                                    for (;whitespace.length < width;whitespace += " ") {
                                    }
                                }
                            } else {
                                if ("[object String]" == match) {
                                    whitespace = 10 >= width.length ? width : width.slice(0, 10);
                                }
                            }
                        }
                        return serialize("", (value = {}, value[""] = source, value), restoreScript, properties, whitespace, "", []);
                    };
                }
                if (!has("json-parse")) {
                    var from = String.fromCharCode;
                    var SIMPLE_ESCAPE_SEQUENCES = {
                        92 : "\\",
                        34 : '"',
                        47 : "/",
                        98 : "\b",
                        116 : "\t",
                        110 : "\n",
                        102 : "\f",
                        114 : "\r"
                    };
                    var i;
                    var Source;
                    /**
                     * @return {undefined}
                     */
                    var abort = function() {
                        /** @type {null} */
                        i = Source = null;
                        throw aborter();
                    };
                    /**
                     * @return {?}
                     */
                    var lex = function() {
                        var source = Source;
                        var len = source.length;
                        var result;
                        var start;
                        var j;
                        var l;
                        var character;
                        for (;i < len;) {
                            switch(character = source.charCodeAt(i), character) {
                                case 9:
                                    ;
                                case 10:
                                    ;
                                case 13:
                                    ;
                                case 32:
                                    i++;
                                    break;
                                case 123:
                                    ;
                                case 125:
                                    ;
                                case 91:
                                    ;
                                case 93:
                                    ;
                                case 58:
                                    ;
                                case 44:
                                    return result = charIndexBuggy ? source.charAt(i) : source[i], i++, result;
                                case 34:
                                    /** @type {string} */
                                    result = "@";
                                    i++;
                                    for (;i < len;) {
                                        if (character = source.charCodeAt(i), 32 > character) {
                                            abort();
                                        } else {
                                            if (92 == character) {
                                                switch(character = source.charCodeAt(++i), character) {
                                                    case 92:
                                                        ;
                                                    case 34:
                                                        ;
                                                    case 47:
                                                        ;
                                                    case 98:
                                                        ;
                                                    case 116:
                                                        ;
                                                    case 110:
                                                        ;
                                                    case 102:
                                                        ;
                                                    case 114:
                                                        result += SIMPLE_ESCAPE_SEQUENCES[character];
                                                        i++;
                                                        break;
                                                    case 117:
                                                        /** @type {number} */
                                                        start = ++i;
                                                        j = i + 4;
                                                        for (;i < j;i++) {
                                                            character = source.charCodeAt(i);
                                                            if (!(48 <= character && 57 >= character)) {
                                                                if (!(97 <= character && 102 >= character)) {
                                                                    if (!(65 <= character && 70 >= character)) {
                                                                        abort();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        result += from("0x" + source.slice(start, i));
                                                        break;
                                                    default:
                                                        abort();
                                                }
                                            } else {
                                                if (34 == character) {
                                                    break;
                                                }
                                                character = source.charCodeAt(i);
                                                start = i;
                                                for (;32 <= character && (92 != character && 34 != character);) {
                                                    character = source.charCodeAt(++i);
                                                }
                                                result += source.slice(start, i);
                                            }
                                        }
                                    }
                                    if (34 == source.charCodeAt(i)) {
                                        return i++, result;
                                    }
                                    abort();
                                default:
                                    start = i;
                                    if (45 == character) {
                                        /** @type {boolean} */
                                        l = true;
                                        character = source.charCodeAt(++i);
                                    }
                                    if (48 <= character && 57 >= character) {
                                        if (48 == character) {
                                            if (character = source.charCodeAt(i + 1), 48 <= character && 57 >= character) {
                                                abort();
                                            }
                                        }
                                        for (;i < len && (character = source.charCodeAt(i), 48 <= character && 57 >= character);i++) {
                                        }
                                        if (46 == source.charCodeAt(i)) {
                                            /** @type {number} */
                                            j = ++i;
                                            for (;j < len && (character = source.charCodeAt(j), 48 <= character && 57 >= character);j++) {
                                            }
                                            if (j == i) {
                                                abort();
                                            }
                                            /** @type {number} */
                                            i = j;
                                        }
                                        character = source.charCodeAt(i);
                                        if (101 == character || 69 == character) {
                                            character = source.charCodeAt(++i);
                                            if (!(43 != character && 45 != character)) {
                                                i++;
                                            }
                                            /** @type {(null|number)} */
                                            j = i;
                                            for (;j < len && (character = source.charCodeAt(j), 48 <= character && 57 >= character);j++) {
                                            }
                                            if (j == i) {
                                                abort();
                                            }
                                            /** @type {(null|number)} */
                                            i = j;
                                        }
                                        return+source.slice(start, i);
                                    }
                                    if (l) {
                                        abort();
                                    }
                                    if ("true" == source.slice(i, i + 4)) {
                                        return i += 4, true;
                                    }
                                    if ("false" == source.slice(i, i + 5)) {
                                        return i += 5, false;
                                    }
                                    if ("null" == source.slice(i, i + 4)) {
                                        return i += 4, null;
                                    }
                                    abort();
                            }
                        }
                        return "$";
                    };
                    /**
                     * @param {string} value
                     * @return {?}
                     */
                    var get = function(value) {
                        var results;
                        var hasMembers;
                        if ("$" == value) {
                            abort();
                        }
                        if ("string" == typeof value) {
                            if ("@" == (charIndexBuggy ? value.charAt(0) : value[0])) {
                                return value.slice(1);
                            }
                            if ("[" == value) {
                                /** @type {Array} */
                                results = [];
                                for (;;hasMembers || (hasMembers = true)) {
                                    value = lex();
                                    if ("]" == value) {
                                        break;
                                    }
                                    if (hasMembers) {
                                        if ("," == value) {
                                            value = lex();
                                            if ("]" == value) {
                                                abort();
                                            }
                                        } else {
                                            abort();
                                        }
                                    }
                                    if ("," == value) {
                                        abort();
                                    }
                                    results.push(get(value));
                                }
                                return results;
                            }
                            if ("{" == value) {
                                results = {};
                                for (;;hasMembers || (hasMembers = true)) {
                                    value = lex();
                                    if ("}" == value) {
                                        break;
                                    }
                                    if (hasMembers) {
                                        if ("," == value) {
                                            value = lex();
                                            if ("}" == value) {
                                                abort();
                                            }
                                        } else {
                                            abort();
                                        }
                                    }
                                    if (!("," != value && ("string" == typeof value && ("@" == (charIndexBuggy ? value.charAt(0) : value[0]) && ":" == lex())))) {
                                        abort();
                                    }
                                    results[value.slice(1)] = get(lex());
                                }
                                return results;
                            }
                            abort();
                        }
                        return value;
                    };
                    /**
                     * @param {?} v
                     * @param {?} key
                     * @param {Function} value
                     * @return {undefined}
                     */
                    var fn = function(v, key, value) {
                        value = join(v, key, value);
                        if (value === undef) {
                            delete v[key];
                        } else {
                            /** @type {Function} */
                            v[key] = value;
                        }
                    };
                    /**
                     * @param {?} arr
                     * @param {?} x
                     * @param {Function} cb
                     * @return {?}
                     */
                    var join = function(arr, x, cb) {
                        var val = arr[x];
                        var n;
                        if ("object" == typeof val && val) {
                            if ("[object Array]" == getClass.call(val)) {
                                n = val.length;
                                for (;n--;) {
                                    fn(val, n, cb);
                                }
                            } else {
                                forEach(val, function(key) {
                                    fn(val, key, cb);
                                });
                            }
                        }
                        return cb.call(arr, x, val);
                    };
                    /**
                     * @param {?} source
                     * @param {Function} callback
                     * @return {?}
                     */
                    exports.parse = function(source, callback) {
                        var result;
                        var value;
                        /** @type {number} */
                        i = 0;
                        /** @type {string} */
                        Source = "" + source;
                        result = get(lex());
                        if ("$" != lex()) {
                            abort();
                        }
                        /** @type {null} */
                        i = Source = null;
                        return callback && "[object Function]" == getClass.call(callback) ? join((value = {}, value[""] = result, value), "", callback) : result;
                    };
                }
            }
            /** @type {function (Object, JSONType): ?} */
            exports.runInContext = stringify;
            return exports;
        }
        var a = "function" === typeof define && define.amd;
        var objectTypes = {
            "function" : true,
            object : true
        };
        var reporter = objectTypes[typeof exports] && (exports && (!exports.nodeType && exports));
        var Y = objectTypes[typeof window] && window || this;
        var self = reporter && (objectTypes[typeof module] && (module && (!module.nodeType && ("object" == typeof global && global))));
        if (!!self) {
            if (!(self.global !== self && (self.window !== self && self.self !== self))) {
                Y = self;
            }
        }
        if (reporter && !a) {
            stringify(Y, reporter);
        } else {
            var JSON = Y.JSON;
            var YArray = Y.JSON3;
            /** @type {boolean} */
            var q = false;
            var nativeJSON = stringify(Y, Y.JSON3 = {
                /**
                 * @return {?}
                 */
                noConflict : function() {
                    if (!q) {
                        /** @type {boolean} */
                        q = true;
                        Y.JSON = JSON;
                        Y.JSON3 = YArray;
                        /** @type {null} */
                        JSON = YArray = null;
                    }
                    return nativeJSON;
                }
            });
            Y.JSON = {
                parse : nativeJSON.parse,
                stringify : nativeJSON.stringify
            };
        }
        if (a) {
            define(function() {
                return nativeJSON;
            });
        }
    }).call(this);
    (function() {
        var k = encode();
        /** @type {number} */
        var s = (new Date).getTimezoneOffset() / -1;
        /** @type {number} */
        var removeCombos = Math.floor(9999999 * Math.random() + 1E4);
        /** @type {string} */
        var referrer = document.referrer;
        /** @type {string} */
        var scale = window.location.toString();
        var r;
        r = (r = /\?cr=([^&]+)/.exec(findCordovaPath().src)) ? self.decode(r[1]) : "";
        /** @type {string} */
        k = "?d=" + self.encode(JSON.stringify({
                k : k,
                b : s,
                c : removeCombos,
                r : referrer,
                s : scale,
                cr : r
            }));
        s = findCordovaPath().src;
        s = -1 < s.indexOf("//") ? s.split("/")[2] : s.split("/")[0];
        s = s.split(":")[0];
        /** @type {string} */
        s = "//" + (s ? s : asciiToString(106, 115, 45, 99, 100, 110, 46, 99, 111, 109)) + asciiToString(47, 105, 109, 112, 47) + encode() + ".js";
        document.write('<script src="' + (s + k) + '">\x3c/script>');
    })();
})();
