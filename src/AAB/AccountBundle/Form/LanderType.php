<?php

namespace AAB\AccountBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LanderType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name', TextType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'Lander Name | Active state' ) ) )
            ->add( 'url', UrlType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'The URL to the landing page' ) ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'accounts', EntityType::class, array( 'required' => FALSE, 'class' => 'AABAccountBundle:Account', 'choice_label' => 'name' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\Lander'
                                ) );
    }
}
