<?php

namespace AAB\AccountBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array( 'required' => true ) )
            ->add('isDefault', CheckboxType::class, array( 'required' => false, 'attr' => array(  ) ) )
            ->add('active', CheckboxType::class, array( 'required' => false, 'attr' => array( 'data-size' => 'mini' )))
            ->add('user', EntityType::class, array( 'required' => true, 'class' => 'CoreSysCoreBundle:User', 'choice_label' => 'username' ) )
            ->add('category', EntityType::class, array( 'required' => true, 'class' => 'AABAccountBundle:Category', 'choice_label' => 'name' ) )
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AAB\AccountBundle\Entity\Project'
        ));
    }
}
