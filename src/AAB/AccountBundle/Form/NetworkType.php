<?php

namespace AAB\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NetworkType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'Network Name | Active state') ) )
            ->add( 'autoFollow', CheckboxType::class, array( 'required' => FALSE ) )
            ->add( 'followsPerDay', ChoiceType::class, array( 'required' => TRUE, 'choices' => $this->getIntegerChoices( 10, 500, 10 ) ) )
            ->add( 'unfollowsPerDay', ChoiceType::class, array( 'required' => TRUE, 'choices' => $this->getIntegerChoices( 10, 500, 10 ) ) )
            ->add( 'maxPostsPerDay', ChoiceType::class, array( 'required' => TRUE, 'choices' => $this->getIntegerChoices( 1, 48 ) ) )
            ->add( 'maxQueriesPerDay', ChoiceType::class, array( 'required' => TRUE, 'choices' => $this->getIntegerChoices( 1, 48 ) ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'adultContent', CheckboxType::class, array( 'required' => FALSE ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\Network'
                                ) );
    }

    /**
     * @param int $start
     * @param int $end
     * @param int $delim
     *
     * @return array
     */
    public function getIntegerChoices( $start = 0, $end = 100, $delim = 1 )
    {
        $return = array();
        for ( $i = $start; $i <= $end; $i += $delim ) {
            $return[ $i ] = $i;
        }

        return $return;
    }
}
