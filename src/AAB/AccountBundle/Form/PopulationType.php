<?php

namespace AAB\AccountBundle\Form;

use Doctrine\DBAL\Types\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PopulationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'follow', CheckboxType::class, array( 'required' => FALSE ) )
            ->add( 'following', CheckboxType::class, array( 'required' => FALSE ) )
            ->add( 'followDate', DateTimeType::class, array( 'required' => FALSE, 'date_widget' => 'single_text' ) )
            ->add( 'followingDate', DateTimeType::class, array( 'required' => FALSE, 'date_widget' => 'single_text' ) )
            ->add( 'account', EntityType::class, array( 'required' => TRUE, 'class' => 'AABAccountBundle:Account', 'choice_label' => 'name' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\Population'
                                ) );
    }
}
