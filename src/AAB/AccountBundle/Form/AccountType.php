<?php

namespace AAB\AccountBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccountType
 * @package AAB\AccountBundle\Form
 */
class AccountType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name', TextType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'The Account Name | Active state' ) ) )
            ->add( 'accountUrl', UrlType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'The Account URL' ) ) )
            ->add( 'postFrequency', ChoiceType::class, array( 'required' => TRUE, 'choices' => $this->getFrequencyChoices(), 'attr' => array( 'help' => '# of posts per day' ) ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'project', EntityType::class, array( 'required' => TRUE, 'class' => 'AABAccountBundle:Project', 'choice_label' => 'title', 'attr' => array( 'help' => 'The project which this account belongs to.' ) ) )
            ->add( 'network', EntityType::class, array( 'required' => TRUE, 'class' => 'AABAccountBundle:Network', 'choice_label' => 'name', 'attr' => array( 'help' => 'The Networks for this acocunt.' ) ) )
            ->add( 'sources', EntityType::class, array( 'required' => FALSE, 'class' => 'AABAccountBundle:Source', 'choice_label' => 'name', 'multiple' => TRUE, 'attr' => array( 'help' => 'The Sources for this account.' ) ) )
            ->add( 'landers', EntityType::class, array( 'required' => FALSE, 'class' => 'AABAccountBundle:Lander', 'choice_label' => 'name', 'multiple' => TRUE, 'attr' => array( 'htlp' => 'The Landers for this account.' ) ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\Account'
                                ) );
    }

    /**
     * @return array
     */
    public function getFrequencyChoices()
    {
        $return = array();
        for ( $i = 1; $i <= 24; $i++ ) {
            $return[ $i ] = $i;
        }

        return $return;
    }
}
