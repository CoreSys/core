<?php

namespace AAB\AccountBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApiKeyType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'appId', TextType::class, array( 'required' => TRUE ) )
            ->add( 'key', TextType::class, array( 'required' => TRUE ) )
            ->add( 'secret', TextType::class, array( 'required' => TRUE ) )
            ->add( 'token', TextType::class, array( 'required' => FALSE ) )
            ->add( 'name', TextType::class, array( 'required' => TRUE ) )
            ->add( 'active', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'data-size' => 'mini' ) ) )
            ->add( 'network', EntityType::class, array( 'required' => TRUE, 'class' => 'AABAccountBundle:Network', 'choice_label' => 'name' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\ApiKey'
                                ) );
    }
}
