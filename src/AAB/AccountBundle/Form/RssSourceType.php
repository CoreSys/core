<?php

namespace AAB\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RssSourceType extends SourceType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        parent::buildForm( $builder, $options );

        $builder
            ->add( 'url', UrlType::class, array( 'required' => TRUE, 'attr' => array( 'help' => 'The url to the RSS Feed') ) )
            ->add( 'titleMap', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The `Title` field name') ) )
            ->add( 'dateMap', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The `Date` field name') ) )
            ->add( 'mediaMap', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The `Media` field name') ) )
            ->add( 'authorMap', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The `Author` field name') ) )
            ->add( 'descMap', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The `Description` field name') ) )
            ->add( 'linkMap', TextType::class, array( 'required' => FALSE, 'attr' => array( 'help' => 'The `Link` field name') ) )
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\RssSource'
                                ) );
    }
}
