<?php

namespace AAB\AccountBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'heading', TextType::class, array( 'required' => TRUE ) )
            ->add( 'content', TextareaType::class, array( 'required' => TRUE ) )
            ->add( 'media', TextType::class, array( 'required' => FALSE ) )
            ->add( 'author', TextType::class, array( 'required' => FALSE ) )
            ->add( 'date', DateTimeType::class, array( 'required' => TRUE, 'date_widget' => 'single_text' ) )
            ->add( 'origin', TextType::class, array( 'required' => FALSE ) )
            ->add( 'postedAt', DateTimeType::class, array( 'required' => TRUE, 'date_widget' => 'single_text' ) )
            ->add( 'source', EntityType::class, array( 'required' => FALSE, 'class' => 'AABAccountBundle:Source', 'multiple' => TRUE, 'choice_label' => 'name' ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'AAB\AccountBundle\Entity\Content'
                                ) );
    }
}
