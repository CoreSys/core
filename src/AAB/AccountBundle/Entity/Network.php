<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Network
 *
 * @ORM\Table(name="aab_network")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\NetworkRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Network",
 *     title="Network Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_aab_",
 *     ajax=@DT\Ajax(true, url="api_aab_post_network_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("createdAt", title="Created", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("updatedAt", title="Last Updated", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("accounts", title="Accounts", className="none", render=@DT\Renderer("renderAccountsCount", template="AABAccountBundle:Renderer:renderAccountsCount.js.twig")),
 *          @DT\Column("apiKeys", title="Api Keys", className="none", render=@DT\Renderer("renderObjectNames", template="AABAccountBundle:Renderer:renderObjectNames.js.twig")),
 *          @DT\Column("adultContent", title="Adult Content", className="", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("autoFollow", title="Auto Follow", className="", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("followsPerDay", title="Follow/Day", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("unFollowsPerDay", title="UnFollow/Day", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("maxPostsPerDay", title="Max Post/Day", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("maxQueriesPerDay", title="Max Queries/Day", className="", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("active",
 *              title="Active",
 *              className="text-center text-xs-center all",
 *              render=@DT\Renderer\CheckboxRenderer(
 *                  click=true,
 *                  confirm="Are you sure?",
 *                  ajax={
 *                      "url"="api_aab_toggle_project",
 *                      "params"={"entity"="id"}
 *                  }
 *              )
 *          )
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_network", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Network")
 *     },
 *     rowActions={
 *          @DT\Action("view", type="link", internal=true, url="aab_admin_network", urlParams={"entity"="id"}, buttonType="info btn-sm", iconClass="fa fa-search", tooltip="View"),
 *          @DT\Action("edit", internal=true, url="api_aab_edit_network", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_network", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Network extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     * @JMS\Expose
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Account", mappedBy="network")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Account>")
     * @JMS\MaxDepth(1)
     */
    private $accounts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ApiKey", mappedBy="network")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\ApiKey>")
     * @JMS\MaxDepth(2)
     */
    private $apiKeys;

    /**
     * @var bool
     *
     * @ORM\Column(name="autoFollow", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $autoFollow;

    /**
     * @var int
     *
     * @ORM\Column(name="followsPerDay", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $followsPerDay;

    /**
     * @var int
     *
     * @ORM\Column(name="unfollowsPerDay", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $unfollowsPerDay;

    /**
     * @var int
     *
     * @ORM\Column(name="maxPostsPerDay", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $maxPostsPerDay;

    /**
     * @var int
     *
     * @ORM\Column(name="maxQueriesPerDay", type="integer", nullable=true)
     * @JMS\Expose
     */
    private $maxQueriesPerDay;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="adultContent", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $adultContent;

    /**
     * Network constructor.
     */
    public function __construct()
    {
        $this->setAccounts( new ArrayCollection() );
        $this->setActive( TRUE );
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setAdultContent( FALSE );
        $this->setApiKeys( new ArrayCollection() );
        $this->setAutoFollow( FALSE );
        $this->setFollowsPerDay( 25 );
        $this->setUnfollowsPerDay( 10 );
        $this->setMaxPostsPerDay( 24 );
        $this->setMaxQueriesPerDay( 24 );
    }

    /**
     * @param ApiKey $apiKey
     *
     * @return $this
     */
    public function addApiKey( ApiKey $apiKey )
    {
        if ( !$this->apiKeys->contains( $apiKey ) ) {
            $this->apiKeys->add( $apiKey );
        }

        return $this;
    }

    /**
     * @param ApiKey $apiKey
     *
     * @return $this
     */
    public function removeApiKey( ApiKey $apiKey )
    {
        if ( $this->apiKey->contains( $apiKey ) ) {
            $this->apiKeys->removeElement( $apiKey );
        }

        return $this;
    }

    /**
     * @param Account $account
     * @param bool    $rev
     *
     * @return $this
     */
    public function addAccount( Account $account, $rev = FALSE )
    {
        if ( !$this->accounts->contains( $account ) ) {
            $this->accounts->add( $account );
            if ( $rev ) {
                $account->setNetwork( $this );
            }
        }

        return $this;
    }

    /**
     * @param Account $account
     * @param bool    $rev
     */
    public function removeAccount( Account $account, $rev = FALSE )
    {
        if ( $this->accounts->contains( $account ) ) {
            $this->accounts->removeElement( $account );
            if ( $rev ) {
                $account->setNetwork( NULL );
            }
        }
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Network
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Network
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Network
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Network
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get accounts
     *
     * @return ArrayCollection
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * Set Accounts
     *
     * @param ArrayCollection $accounts
     *
     * @return Network
     */
    public function setAccounts( $accounts = NULL )
    {
        $this->accounts = $accounts;

        return $this;
    }

    /**
     * Get apiKeys
     *
     * @return ArrayCollection
     */
    public function getApiKeys()
    {
        return $this->apiKeys;
    }

    /**
     * Set ApiKeys
     *
     * @param ArrayCollection $apiKeys
     *
     * @return Network
     */
    public function setApiKeys( $apiKeys = NULL )
    {
        $this->apiKeys = $apiKeys;

        return $this;
    }

    /**
     * Get autoFollow
     *
     * @return boolean
     */
    public function getAutoFollow()
    {
        return $this->autoFollow === TRUE;
    }

    /**
     * Set AutoFollow
     *
     * @param boolean $autoFollow
     *
     * @return Network
     */
    public function setAutoFollow( $autoFollow = TRUE )
    {
        $this->autoFollow = $autoFollow === TRUE;

        return $this;
    }

    /**
     * Get followsPerDay
     *
     * @return int
     */
    public function getFollowsPerDay()
    {
        return intval( $this->followsPerDay );
    }

    /**
     * Set FollowsPerDay
     *
     * @param int $followsPerDay
     *
     * @return Network
     */
    public function setFollowsPerDay( $followsPerDay = 0 )
    {
        $this->followsPerDay = intval( $followsPerDay );

        return $this;
    }

    /**
     * Get unfollowsPerDay
     *
     * @return int
     */
    public function getUnfollowsPerDay()
    {
        return intval( $this->unfollowsPerDay );
    }

    /**
     * Set UnfollowsPerDay
     *
     * @param int $unfollowsPerDay
     *
     * @return Network
     */
    public function setUnfollowsPerDay( $unfollowsPerDay = 0 )
    {
        $this->unfollowsPerDay = intval( $unfollowsPerDay );

        return $this;
    }

    /**
     * Get maxPostsPerDay
     *
     * @return int
     */
    public function getMaxPostsPerDay()
    {
        return intval( $this->maxPostsPerDay );
    }

    /**
     * Set MaxPostsPerDay
     *
     * @param int $maxPostsPerDay
     *
     * @return Network
     */
    public function setMaxPostsPerDay( $maxPostsPerDay = 0 )
    {
        $this->maxPostsPerDay = intval( $maxPostsPerDay );

        return $this;
    }

    /**
     * Get maxQueriesPerDay
     *
     * @return int
     */
    public function getMaxQueriesPerDay()
    {
        return intval( $this->maxQueriesPerDay );
    }

    /**
     * Set MaxQueriesPerDay
     *
     * @param int $maxQueriesPerDay
     *
     * @return Network
     */
    public function setMaxQueriesPerDay( $maxQueriesPerDay = 0 )
    {
        $this->maxQueriesPerDay = intval( $maxQueriesPerDay );

        return $this;
    }

    /**
     * Get adultContent
     *
     * @return boolean
     */
    public function getAdultContent()
    {
        return $this->adultContent === TRUE;
    }

    /**
     * Set AdultContent
     *
     * @param boolean $adultContent
     *
     * @return Network
     */
    public function setAdultContent( $adultContent = TRUE )
    {
        $this->adultContent = $adultContent === TRUE;

        return $this;
    }

}

