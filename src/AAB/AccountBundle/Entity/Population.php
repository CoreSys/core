<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Population
 *
 * @ORM\Table(name="aab_population")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\PopulationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Population extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="follow", type="boolean")
     */
    private $follow;

    /**
     * @var bool
     *
     * @ORM\Column(name="following", type="boolean")
     */
    private $following;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="followDate", type="datetime")
     */
    private $followDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="followingDate", type="datetime")
     */
    private $followingDate;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="populations")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private $account;

    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setFollow( FALSE );
        $this->setFollowing( FALSE );
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Population
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Population
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get follow
     *
     * @return boolean
     */
    public function getFollow()
    {
        return $this->follow === TRUE;
    }

    /**
     * Set Follow
     *
     * @param boolean $follow
     *
     * @return Population
     */
    public function setFollow( $follow = TRUE )
    {
        $this->follow = $follow === TRUE;

        return $this;
    }

    /**
     * Get following
     *
     * @return boolean
     */
    public function getFollowing()
    {
        return $this->following === TRUE;
    }

    /**
     * Set Following
     *
     * @param boolean $following
     *
     * @return Population
     */
    public function setFollowing( $following = TRUE )
    {
        $this->following = $following === TRUE;

        return $this;
    }

    /**
     * Get followDate
     *
     * @return \DateTime
     */
    public function getFollowDate()
    {
        return $this->followDate;
    }

    /**
     * Set FollowDate
     *
     * @param \DateTime $followDate
     *
     * @return Population
     */
    public function setFollowDate( $followDate = NULL )
    {
        $this->followDate = $followDate;

        return $this;
    }

    /**
     * Get followingDate
     *
     * @return \DateTime
     */
    public function getFollowingDate()
    {
        return $this->followingDate;
    }

    /**
     * Set FollowingDate
     *
     * @param \DateTime $followingDate
     *
     * @return Population
     */
    public function setFollowingDate( $followingDate = NULL )
    {
        $this->followingDate = $followingDate;

        return $this;
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set Account
     *
     * @param Account $account
     *
     * @return Population
     */
    public function setAccount( $account = NULL )
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }
}

