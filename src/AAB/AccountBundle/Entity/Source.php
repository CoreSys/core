<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Source
 *
 * @ORM\Table(name="source")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\SourceRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"manual"="Source", "rss"="RssSource"})
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Source",
 *     title="Manual Source Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_aab_",
 *     ajax=@DT\Ajax(true, url="api_aab_post_source_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("createdAt", title="Created", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("updatedAt", title="Last Updated", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("type", title="Type", className="all"),
 *          @DT\Column("active", title="Active", className="all", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("accounts", title="Accounts", className="all", render=@DT\Renderer("renderAccountNames", template="AABAccountBundle:Renderer:renderAccountNames.js.twig")),
 *          @DT\Column("contentsCount", title="Contents Count", className="all")
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_source", buttonType="success", iconClass="fa fa-plus-circle", text="Create Source")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_aab_edit_source", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_source", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Source extends BaseEntity
{

    /**
     * @var string
     * @JMS\Expose
     */
    protected $type = 'manual';

    /**
     * @var string
     *
     * @ORM\Column(length=12, nullable=true)
     */
    protected $sourceType = 'manual';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Account", mappedBy="sources")
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Account>")
     */
    private $accounts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Content", mappedBy="source")
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Content>")
     */
    private $contents;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(length=64, nullable=true)
     * @JMS\Expose
     */
    private $name;

    /**
     * Source constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setActive( TRUE );
        $this->setAccounts( new ArrayCollection() );
        $this->setContents( new ArrayCollection() );
        $this->setSourceType( 'manual' );
    }

    /**
     * @JMS\VirtualProperty
     * @return int
     */
    public function getContentsCount()
    {
        $contents = $this->getContents();

        return count( $contents );
    }

    /**
     * Get type
     *
     * @JMS\VirtualProperty
     * @return mixed
     */
    public function getType()
    {
        return empty( $this->type ) ? 'source' : $this->type;
    }

    /**
     * Set Type
     *
     * @param mixed $type
     *
     * @return Source
     */
    public function setType( $type = NULL )
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Source
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get sourceType
     *
     * @return string
     */
    public function getSourceType()
    {
        return $this->sourceType;
    }

    /**
     * Set SourceType
     *
     * @param string $sourceType
     *
     * @return Source
     */
    public function setSourceType( $sourceType = NULL )
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
        $this->setSourceType( 'manual' );
    }

    /**
     * @param Account $account
     *
     * @return $this
     */
    public function addAccount( Account $account )
    {
        if ( !$this->accounts->contains( $account ) ) {
            $this->accounts->add( $account );
        }

        return $this;
    }

    /**
     * @param Content $content
     *
     * @return $this
     */
    public function addContent( Content $content )
    {
        if ( !$this->contents->contains( $content ) ) {
            $this->contents->add( $content );
        }

        return $this;
    }

    /**
     * @param Account $account
     *
     * @return $this
     */
    public function removeAccount( Account $account )
    {
        if ( $this->accounts->contains( $account ) ) {
            $this->accounts->removeElement( $account );
        }

        return $this;
    }

    /**
     * @param Content $content
     *
     * @return $this
     */
    public function removeContent( Content $content )
    {
        if ( $this->contents->contains( $content ) ) {
            $this->contents->removeElement( $content );
        }

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Source
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Source
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Source
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get accounts
     *
     * @return ArrayCollection
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * Set Accounts
     *
     * @param ArrayCollection $accounts
     *
     * @return Source
     */
    public function setAccounts( $accounts = NULL )
    {
        $this->accounts = $accounts;

        return $this;
    }

    /**
     * Get contents
     *
     * @return ArrayCollection
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Set Contents
     *
     * @param ArrayCollection $contents
     *
     * @return Source
     */
    public function setContents( $contents = NULL )
    {
        $this->contents = $contents;

        return $this;
    }
}

