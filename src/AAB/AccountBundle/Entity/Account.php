<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Account
 *
 * @ORM\Table(name="aab_account")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\AccountRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Account",
 *     title="Account Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_aab_",
 *     createdRow="accountRowCreated",
 *     ajax=@DT\Ajax(true, url="api_aab_post_account_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("accountUrl", title="URL", className="", render=@DT\Renderer\UrlRenderer()),
 *          @DT\Column("postFrequency", title="Post Frequency", className="all", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("project", orderable=false, searchable=false, title="Project", className="all", render=@DT\Renderer("renderProjectName", template="AABAccountBundle:Renderer:renderProjectName.js.twig", urlName="aab_admin_project", urlParams={"id"="id"})),
 *          @DT\Column("createdAt", title="Created", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("updatedAt", title="Last Updated", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("network", title="Network", className="none", render=@DT\Renderer("renderObjectName", template="AABAccountBundle:Renderer:renderObjectName.js.twig")),
 *          @DT\Column("authorizedApp", title="Authorized", className="none", render=@DT\Renderer("renderObjectName", template="AABAccountBundle:Renderer:renderObjectName.js.twig")),
 *          @DT\Column("populations", title="Populations", className="none", render=@DT\Renderer("renderPopulations", template="AABAccountBundle:Renderer:renderPopulations.js.twig")),
 *          @DT\Column("sources", title="Sources", className="none", render=@DT\Renderer("renderSources", template="AABAccountBundle:Renderer:renderSources.js.twig")),
 *          @DT\Column("landers", title="Landers", className="none", render=@DT\Renderer("renderLanders", template="AABAccountBundle:Renderer:renderLanders.js.twig")),
 *          @DT\Column("active",
 *              title="Active",
 *              className="text-center text-xs-center all",
 *              render=@DT\Renderer\CheckboxRenderer(
 *                  click=true,
 *                  confirm="Are you sure?",
 *                  ajax={
 *                      "url"="api_aab_toggle_project",
 *                      "params"={"entity"="id"}
 *                  }
 *              )
 *          )
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_account", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Account")
 *     },
 *     rowActions={
 *          @DT\Action("view", type="link", internal=true, url="aab_admin_account", urlParams={"entity"="id"}, buttonType="info btn-sm", iconClass="fa fa-search", tooltip="View"),
 *          @DT\Action("edit", internal=true, url="api_aab_edit_account", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_account", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Account extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="accounts")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     * @JMS\Expose
     * @JMS\Type("AAB\AccountBundle\Entity\Project")
     * @JMS\MaxDepth(2)
     */
    private $project;

    /**
     * @var Network
     *
     * @ORM\ManyToOne(targetEntity="Network", inversedBy="accounts")
     * @ORM\JoinColumn(name="network_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @JMS\Expose
     * @JMS\Type("AAB\AccountBundle\Entity\Network")
     * @JMS\MaxDepth(2)
     */
    private $network;

    /**
     * @var ApiKey
     *
     * @ORM\ManyToOne(targetEntity="ApiKey")
     * @ORM\JoinColumn(name="authorizedApp_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\Type("AAB\AccountBundle\Entity\ApiKey")
     * @JMS\MaxDepth(2)
     */
    private $authorizedApp;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Population", mappedBy="account")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Population>")
     * @JMS\MaxDepth(2)
     */
    private $populations;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     * @JMS\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="accountUrl", type="string", length=255)
     * @JMS\Expose
     */
    private $accountUrl;

    /**
     * @var int
     *
     * @ORM\Column(name="postFrequency", type="integer")
     * @JMS\Expose
     */
    private $postFrequency;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Source", inversedBy="accounts")
     * @ORM\JoinTable(name="aab_account_sources")
     * @JMS\Expose
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Source>")
     */
    private $sources;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Lander", inversedBy="accounts")
     * @ORM\JoinTable(name="aab_account_landers")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Lander>")
     * @JMS\MaxDepth(2)
     */
    private $landers;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setCreatedAt( new \DateTime() );
        $this->setLanders( new ArrayCollection() );
        $this->setPopulations( new ArrayCollection() );
        $this->setPostFrequency( 6 );
        $this->setSources( new ArrayCollection() );
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Account
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * @param Lander $lander
     * @param bool   $rev
     *
     * @return $this
     */
    public function addLander( Lander $lander, $rev = TRUE )
    {
        if ( !$this->landers->contains( $lander ) ) {
            $this->landers->add( $lander );
            if ( $rev ) {
                $lander->addAccount( $this, FALSE );
            }
        }

        return $this;
    }

    /**
     * @param Lander $lander
     * @param bool   $rev
     *
     * @return $this
     */
    public function removeLander( Lander $lander, $rev = TRUE )
    {
        if ( $this->landers->contains( $lander ) ) {
            $this->landers->removeElement( $lander );
            if ( $rev ) {
                $lander->removeAccount( $this, FALSE );
            }
        }

        return $this;
    }

    /**
     * @param Population $population
     *
     * @return $this
     */
    public function addPopulation( Population $population )
    {
        if ( !$this->populations->contains( $population ) ) {
            $this->populations->add( $population );
            $population->setAccount( $this );
        }

        return $this;
    }

    /**
     * @param Population $population
     *
     * @return $this
     */
    public function removePopulation( Population $population )
    {
        if ( $this->populations->contains( $population ) ) {
            $this->populations->removeElement( $population );
            $population->setAccount( NULL );
        }

        return $this;
    }

    /**
     * @param Source $source
     * @param bool   $rev
     *
     * @return $this
     */
    public function addSource( Source $source, $rev = TRUE )
    {
        if ( !$this->sources->contains( $source ) ) {
            $this->sources->add( $source );
            if ( $rev ) {
                $source->addAccount( $this, FALSE );
            }
        }

        return $this;
    }

    /**
     * @param Source $source
     * @param bool   $rev
     *
     * @return $this
     */
    public function removeSource( Source $source, $rev = TRUE )
    {
        if ( $this->sources->contains( $source ) ) {
            $this->sources->removeElement( $source );
            if ( $rev ) {
                $source->removeAccount( $this, FALSE );
            }
        }

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Account
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param string $updatedAt
     *
     * @return Account
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set Project
     *
     * @param Project $project
     *
     * @return Account
     */
    public function setProject( $project = NULL )
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get network
     *
     * @return Network
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set Network
     *
     * @param Network $network
     *
     * @return Account
     */
    public function setNetwork( $network = NULL )
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get authorizedApp
     *
     * @return ApiKey
     */
    public function getAuthorizedApp()
    {
        return $this->authorizedApp;
    }

    /**
     * Set AuthorizedApp
     *
     * @param ApiKey $authorizedApp
     *
     * @return Account
     */
    public function setAuthorizedApp( $authorizedApp = NULL )
    {
        $this->authorizedApp = $authorizedApp;

        return $this;
    }

    /**
     * Get populations
     *
     * @return ArrayCollection
     */
    public function getPopulations()
    {
        return $this->populations;
    }

    /**
     * Set Populations
     *
     * @param ArrayCollection $populations
     *
     * @return Account
     */
    public function setPopulations( $populations = NULL )
    {
        $this->populations = $populations;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Account
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get accountUrl
     *
     * @return string
     */
    public function getAccountUrl()
    {
        return $this->accountUrl;
    }

    /**
     * Set AccountUrl
     *
     * @param string $accountUrl
     *
     * @return Account
     */
    public function setAccountUrl( $accountUrl = NULL )
    {
        $this->accountUrl = $accountUrl;

        return $this;
    }

    /**
     * Get postFrequency
     *
     * @return int
     */
    public function getPostFrequency()
    {
        return intval( $this->postFrequency );
    }

    /**
     * Set PostFrequency
     *
     * @param int $postFrequency
     *
     * @return Account
     */
    public function setPostFrequency( $postFrequency = 0 )
    {
        $this->postFrequency = intval( $postFrequency );

        return $this;
    }

    /**
     * Get sources
     *
     * @return ArrayCollection
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * Set Sources
     *
     * @param ArrayCollection $sources
     *
     * @return Account
     */
    public function setSources( $sources = NULL )
    {
        $this->sources = $sources;

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @return ArrayCollection
     */
    public function getBaseSources()
    {
        $return = new ArrayCollection();

        foreach ( $this->getSources() as $src ) {
            if ( $src instanceof Source && !$src instanceof RssSource ) {
                $return->add( $src );
            }
        }

        return $return;
    }

    /**
     * Get landers
     *
     * @return ArrayCollection
     */
    public function getLanders()
    {
        return $this->landers;
    }

    /**
     * Set Landers
     *
     * @param ArrayCollection $landers
     *
     * @return Account
     */
    public function setLanders( $landers = NULL )
    {
        $this->landers = $landers;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * @param $source_id
     *
     * @return bool
     */
    public function hasSourceId( $source_id )
    {
        foreach ( $this->getSources() as $source ) {
            if ( $source->getId() === $source_id ) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * @JMS\VirtualProperty
     * @return ArrayCollection
     */
    public function getRssSources()
    {
        $return = new ArrayCollection();

        foreach ( $this->getSources() as $src ) {
            if ( $src instanceof RssSource ) {
                $return->add( $src );
            }
        }

        return $return;
    }
}

