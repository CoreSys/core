<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Category
 *
 * @ORM\Table(name="aab_category")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Category",
 *     title="Category Management",
 *     responsive=true,
 *     checkable=false,
 *     ajax=@DT\Ajax(true, url="api_aab_post_category_datatables"),
 *     apiBase="api_aab_",
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("description", title="Description", className="")
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_category", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Category")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_aab_edit_category", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_category", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Category extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     * @JMS\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Expose
     */
    private $description;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        // do nothing
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription( $description = NULL )
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }
}

