<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Lander
 *
 * @ORM\Table(name="aab_lander")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\LanderRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Lander",
 *     title="Landers Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_aab_",
 *     ajax=@DT\Ajax(true, url="api_aab_post_lander_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("createdAt", title="Created", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("updatedAt", title="Last Updated", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("accounts", title="Accounts", className="none", render=@DT\Renderer("renderAccountsCount", template="AABAccountBundle:Renderer:renderAccountsCount.js.twig")),
 *          @DT\Column("url", title="URL", className="all", render=@DT\Renderer\UrlRenderer()),
 *          @DT\Column("active", title="Active", className="text-center text-xs-center all", render=@DT\Renderer\CheckboxRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_lander", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Lander")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_aab_edit_lander", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_lander", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Lander extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @JMS\Expose
     * @@JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime<'M d, Y g:i a'>")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     * @JMS\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @JMS\Expose
     */
    private $url;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Account", mappedBy="landers")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Account>")
     * @JMS\MaxDepth(2)
     */
    private $accounts;

    /**
     * Lander constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setActive( TRUE );
        $this->setAccounts( new ArrayCollection() );
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * @param Account $account
     * @param bool    $rev
     *
     * @return $this
     */
    public function addAccount( Account $account, $rev = TRUE )
    {
        if ( !$this->accounts->contains( $account ) ) {
            $this->accounts->add( $account );
            if ( $rev ) {
                $account->addLander( $this, FALSE );
            }
        }

        return $this;
    }

    /**
     * @param Account $account
     * @param bool    $rev
     *
     * @return $this
     */
    public function removeAccount( Account $account, $rev = TRUE )
    {
        if ( $this->accounts->contains( $account ) ) {
            $this->accounts->removeElement( $account );
            if ( $rev ) {
                $account->removeLander( $this, FALSE );
            }
        }

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Lander
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Lander
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return Lander
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set Url
     *
     * @param string $url
     *
     * @return Lander
     */
    public function setUrl( $url = NULL )
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Lander
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get accounts
     *
     * @return ArrayCollection
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * Set Accounts
     *
     * @param ArrayCollection $accounts
     *
     * @return Lander
     */
    public function setAccounts( $accounts = NULL )
    {
        $this->accounts = $accounts;

        return $this;
    }
}

