<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * Project
 *
 * @ORM\Table(name="aab_project")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\ProjectRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("Project",
 *     title="Project Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_aab_",
 *     createdRow="projectRowCreated",
 *     ajax=@DT\Ajax(true, url="api_aab_post_project_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("user", title="User", className="all", render=@DT\Renderer("renderProjectUser", template="AABAccountBundle:Renderer:renderProjectUser.js.twig")),
 *          @DT\Column("title", title="Title", className="all"),
 *          @DT\Column("category", title="Category", className="", render=@DT\Renderer("renderObjectName", template="AABAccountBundle:Renderer:renderObjectName.js.twig")),
 *          @DT\Column("accounts", title="Accounts", className="none", render=@DT\Renderer("renderAccountNames", template="AABAccountBundle:Renderer:renderAccountNames.js.twig")),
 *          @DT\Column("active",
 *              title="Active",
 *              className="text-center text-xs-center all",
 *              render=@DT\Renderer\CheckboxRenderer(
 *                  click=true,
 *                  confirm="Are you sure?",
 *                  ajax={
 *                      "url"="api_aab_toggle_project",
 *                      "params"={"entity"="id"}
 *                  }
 *              )
 *          ),
 *          @DT\Column("isDefault", title="Default", className="text-center text-xs-center all", render=@DT\Renderer\CheckboxRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_project", buttonType="success", iconClass="fa fa-plus-circle", text="Create New Project")
 *     },
 *     rowActions={
 *          @DT\Action("view", type="link", internal=true, url="aab_admin_project", urlParams={"entity"="id"}, buttonType="info btn-sm", iconClass="fa fa-search", tooltip="View"),
 *          @DT\Action("edit", internal=true, url="api_aab_edit_project", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_project", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class Project extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @JMS\Expose
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @JMS\Expose
     */
    private $updatedAt;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="CoreSys\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * @JMS\Expose
     * @JMS\Type("CoreSys\CoreBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64)
     * @JMS\Expose
     */
    private $title;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Expose
     * @JMS\Type("AAB\AccountBundle\Entity\Category")
     * @JMS\MaxDepth(2)
     */
    private $category;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Account", mappedBy="project")
     * @JMS\Expose
     * @JMS\Type("ArrayCollection<AAB\AccountBundle\Entity\Account>")
     * @JMS\MaxDepth(2)
     */
    private $accounts;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDefault", type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $isDefault;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @JMS\Expose
     */
    private $active;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setIsDefault( FALSE );
        $this->setUser( NULL );
        $this->setAccounts( new ArrayCollection() );
        $this->setCategory( NULL );
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return Project
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return Project
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Project
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return Project
     */
    public function setUser( $user = NULL )
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return Project
     */
    public function setTitle( $title = NULL )
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set Category
     *
     * @param Category $category
     *
     * @return Project
     */
    public function setCategory( $category = NULL )
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get accounts
     *
     * @return ArrayCollection
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * Set Accounts
     *
     * @param ArrayCollection $accounts
     *
     * @return Project
     */
    public function setAccounts( $accounts = NULL )
    {
        $this->accounts = $accounts;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault === TRUE;
    }

    /**
     * Set IsDefault
     *
     * @param boolean $isDefault
     *
     * @return Project
     */
    public function setIsDefault( $isDefault = TRUE )
    {
        $this->isDefault = $isDefault === TRUE;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getTitle();
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        // @TODO
    }

    public function addAccount( Account $account )
    {
        if ( !$this->accounts->contains( $account ) ) {
            $this->accounts->add( $account );
            $account->setProject( $this );
        }

        return $this;
    }

    public function removeAccount( Account $account )
    {
        if ( $this->accounts->contains( $account ) ) {
            $this->accounts->removeElement( $account );
            $account->setProject( NULL );
        }

        return $this;
    }
}

