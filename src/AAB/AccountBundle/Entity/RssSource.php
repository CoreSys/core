<?php

namespace AAB\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use CoreSys\CoreBundle\Annotation\Datatables as DT;

/**
 * RssSource
 *
 * @ORM\Table(name="aab_rss_source")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\RssSourceRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @DT\Table("RssSource",
 *     title="RSS Source Management",
 *     responsive=true,
 *     checkable=false,
 *     apiBase="api_aab_",
 *     ajax=@DT\Ajax(true, url="api_aab_post_rsssource_datatables"),
 *     columns={
 *          @DT\Column("id", title="ID", className="all"),
 *          @DT\Column("name", title="Name", className="all"),
 *          @DT\Column("url", title="URL", className="none", render=@DT\Renderer\UrlRenderer()),
 *          @DT\Column("createdAt", title="Created", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("updatedAt", title="Last Updated", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("type", title="Type", className="all"),
 *          @DT\Column("active", title="Active", className="all", render=@DT\Renderer\CheckboxRenderer()),
 *          @DT\Column("accounts", title="Accounts", className="all", render=@DT\Renderer("renderAccountNames", template="AABAccountBundle:Renderer:renderAccountNames.js.twig")),
 *          @DT\Column("contentsCount", title="Contents Count", className="all"),
 *          @DT\Column("titleMap", title="Title Mapping", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("dateMap", title="Date Mapping", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("authorMap", title="Author Mapping", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("mediaMap", title="Media Mapping", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("descMap", title="Description Mapping", className="none", render=@DT\Renderer\BlankRenderer()),
 *          @DT\Column("linkMap", title="Link Mapping", className="none", render=@DT\Renderer\BlankRenderer())
 *     },
 *     tableActions={
 *          @DT\Action("create", internal=true, url="api_aab_new_rsssource", buttonType="success", iconClass="fa fa-plus-circle", text="Create RSS Source")
 *     },
 *     rowActions={
 *          @DT\Action("edit", internal=true, url="api_aab_edit_rsssource", urlParams={"entity"="id"}, buttonType="warning btn-sm", iconClass="fa fa-pencil", tooltip="Edit"),
 *          @DT\Action("delete", internal=true, url="api_aab_delete_rsssource", urlParams={"entity"="id"}, buttonType="danger btn-sm", iconClass="fa fa-trash-o", tooltip="Delete")
 *     }
 * )
 */
class RssSource extends Source
{

    /**
     * @var string
     * @JMS\Expose
     */
    protected $type = 'rss';

    /**
     * @var string
     *
     * @ORM\Column(name="url", length=255, nullable=true)
     * @JMS\Expose
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="titleMap", type="string", length=64)
     */
    private $titleMap;

    /**
     * @var string
     *
     * @ORM\Column(name="dateMap", type="string", length=64)
     */
    private $dateMap;

    /**
     * @var string
     *
     * @ORM\Column(name="mediaMap", type="string", length=64)
     */
    private $mediaMap;

    /**
     * @var string
     *
     * @ORM\Column(name="authorMap", type="string", length=64)
     */
    private $authorMap;

    /**
     * @var string
     *
     * @ORM\Column(name="descMap", type="string", length=64)
     */
    private $descMap;

    /**
     * @var string
     *
     * @ORM\Column(name="linkMap", type="string", length=64)
     */
    private $linkMap;

    /**
     * RssSource constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTitleMap( 'title' );
        $this->setAuthorMap( 'author' );
        $this->setLinkMap( 'link' );
        $this->setDateMap( 'date' );
        $this->setDescMap( 'description' );
        $this->setMediaMap( 'media' );
        $this->setSourceType( 'rss');
    }

    /**
     * Get url
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set Url
     *
     * @param mixed $url
     *
     * @return RssSource
     */
    public function setUrl( $url = NULL )
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get titleMap
     *
     * @return string
     */
    public function getTitleMap()
    {
        return $this->titleMap;
    }

    /**
     * Set TitleMap
     *
     * @param string $titleMap
     *
     * @return RssSource
     */
    public function setTitleMap( $titleMap = NULL )
    {
        $this->titleMap = $titleMap;

        return $this;
    }

    /**
     * Get dateMap
     *
     * @return string
     */
    public function getDateMap()
    {
        return $this->dateMap;
    }

    /**
     * Set DateMap
     *
     * @param string $dateMap
     *
     * @return RssSource
     */
    public function setDateMap( $dateMap = NULL )
    {
        $this->dateMap = $dateMap;

        return $this;
    }

    /**
     * Get mediaMap
     *
     * @return string
     */
    public function getMediaMap()
    {
        return $this->mediaMap;
    }

    /**
     * Set MediaMap
     *
     * @param string $mediaMap
     *
     * @return RssSource
     */
    public function setMediaMap( $mediaMap = NULL )
    {
        $this->mediaMap = $mediaMap;

        return $this;
    }

    /**
     * Get authorMap
     *
     * @return string
     */
    public function getAuthorMap()
    {
        return $this->authorMap;
    }

    /**
     * Set AuthorMap
     *
     * @param string $authorMap
     *
     * @return RssSource
     */
    public function setAuthorMap( $authorMap = NULL )
    {
        $this->authorMap = $authorMap;

        return $this;
    }

    /**
     * Get descMap
     *
     * @return string
     */
    public function getDescMap()
    {
        return $this->descMap;
    }

    /**
     * Set DescMap
     *
     * @param string $descMap
     *
     * @return RssSource
     */
    public function setDescMap( $descMap = NULL )
    {
        $this->descMap = $descMap;

        return $this;
    }

    /**
     * Get linkMap
     *
     * @return string
     */
    public function getLinkMap()
    {
        return $this->linkMap;
    }

    /**
     * Set LinkMap
     *
     * @param string $linkMap
     *
     * @return RssSource
     */
    public function setLinkMap( $linkMap = NULL )
    {
        $this->linkMap = $linkMap;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setSourceType( 'rss');
    }
}

