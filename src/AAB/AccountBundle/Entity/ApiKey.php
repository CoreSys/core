<?php

namespace AAB\AccountBundle\Entity;

use CoreSys\CoreBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ApiKey
 *
 * @ORM\Table(name="aab_api_key")
 * @ORM\Entity(repositoryClass="AAB\AccountBundle\Repository\ApiKeyRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ApiKey extends BaseEntity
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var Network
     *
     * @ORM\ManyToOne(targetEntity="Network", inversedBy="apiKeys")
     * @ORM\JoinColumn(name="network_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $network;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     */
    private $appId;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     */
    private $secret;

    /**
     * @var string
     *
     * @ORM\Column(length=128, nullable=true)
     */
    private $token;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Account", mappedBy="authorizedApp")
     */
    private $accounts;

    /**
     * @var string
     *
     * @ORM\Column(length=64)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * ApiKey constructor.
     */
    public function __construct()
    {
        $this->setActive( TRUE );
        $this->setCreatedAt( new \DateTime() );
        $this->setUpdatedAt( new \DateTime() );
        $this->setAccounts( new ArrayCollection() );
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set CreatedAt
     *
     * @param \DateTime $createdAt
     *
     * @return ApiKey
     */
    public function setCreatedAt( $createdAt = NULL )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set UpdatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ApiKey
     */
    public function setUpdatedAt( $updatedAt = NULL )
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get network
     *
     * @return Network
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set Network
     *
     * @param Network $network
     *
     * @return ApiKey
     */
    public function setNetwork( $network = NULL )
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get appId
     *
     * @return string
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set AppId
     *
     * @param string $appId
     *
     * @return ApiKey
     */
    public function setAppId( $appId = NULL )
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set Key
     *
     * @param string $key
     *
     * @return ApiKey
     */
    public function setKey( $key = NULL )
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set Secret
     *
     * @param string $secret
     *
     * @return ApiKey
     */
    public function setSecret( $secret = NULL )
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set Token
     *
     * @param string $token
     *
     * @return ApiKey
     */
    public function setToken( $token = NULL )
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get accounts
     *
     * @return ArrayCollection
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * Set Accounts
     *
     * @param ArrayCollection $accounts
     *
     * @return ApiKey
     */
    public function setAccounts( $accounts = NULL )
    {
        $this->accounts = $accounts;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return ApiKey
     */
    public function setName( $name = NULL )
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active === TRUE;
    }

    /**
     * Set Active
     *
     * @param boolean $active
     *
     * @return ApiKey
     */
    public function setActive( $active = TRUE )
    {
        $this->active = $active === TRUE;

        return $this;
    }

    /**
     * @param Account $account
     */
    public function addAccount( Account $account )
    {
        if ( !$this->accounts->contains( $account ) ) {
            $this->accounts->add( $account );
            $account->setAuthorizedApp( $this );
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\PrePersist
     */
    public function prepersist()
    {
        $this->setUpdatedAt( new \DateTime() );
    }
}

