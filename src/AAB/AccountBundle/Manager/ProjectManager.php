<?php

namespace AAB\AccountBundle\Manager;

use AAB\AccountBundle\Entity\Project;
use CoreSys\CoreBundle\Entity\User;
use CoreSys\CoreBundle\Manager\BaseManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ProjectManager
 * @package AAB\AccountBundle\Manager
 * @DI\Service("aab_account.manager.project", parent="core_sys_core.manager.base")
 */
class ProjectManager extends BaseManager
{

    /**
     * @param User  $user
     * @param array $data
     * @param       $save
     *
     * @return Project
     */
    public function createUserProject( User $user, array $data = array(), $save )
    {
        $project = new Project();
        $project->setUser( $user );

        foreach ( $data as $key => $val ) {
            $method = 'set' . str_replace( ' ', '', ucwords( str_replace( '_', ' ', $key ) ) );
            if ( method_exists( $project, $method ) ) {
                $project->$method( $val );
            }
        }

        if ( $save ) {
            $this->getBaseController()->persistAndFlush( $project );
        }

        return $project;
    }
}