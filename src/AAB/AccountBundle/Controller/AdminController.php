<?php

namespace AAB\AccountBundle\Controller;

use AAB\AccountBundle\Entity\Account;
use AAB\AccountBundle\Entity\Category;
use AAB\AccountBundle\Entity\Lander;
use AAB\AccountBundle\Entity\Network;
use AAB\AccountBundle\Entity\Project;
use AAB\AccountBundle\Entity\RssSource;
use AAB\AccountBundle\Entity\Source;
use AAB\AccountBundle\Form\AccountType;
use AAB\AccountBundle\Form\NetworkType;
use AAB\AccountBundle\Form\ProjectType;
use CoreSys\CoreBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package AAB\AccountBundle\Controller
 * @Route("/admin/aab")
 */
class AdminController extends BaseController
{

    /**
     * @Route("/categories", name="aab_admin_categories")
     * @Template()
     */
    public function categoriesAction()
    {
        return array(
            'entity' => new Category()
        );
    }

    /**
     * @Route("/projects", name="aab_admin_projects")
     * @Template
     */
    public function projectsAction()
    {
        return array(
            'entity' => new Project()
        );
    }

    /**
     * @Route("/accounts", name="aab_admin_accounts")
     * @Template
     */
    public function accountsAction()
    {
        return array(
            'entity' => new Account()
        );
    }

    /**
     * @Route("/networks", name="aab_admin_networks")
     * @Template
     */
    public function networksAction()
    {
        return array(
            'entity' => new Network()
        );
    }

    /**
     * @Route("/network/{id}", name="aab_admin_network", defaults={"id"=null}, options={"expose"=true})
     * @Template
     * @ParamConverter("network", class="AABAccountBundle:Network")
     *
     * @param Network $network
     *
     * @return array
     */
    public function networkAction( Network $network )
    {
        $form = $this->createForm( NetworkType::class, $network );

        return array(
            'form'    => $form->createView(),
            'network' => $network
        );
    }

    /**
     * @Route("/project/{id}", name="aab_admin_project", defaults={"id"=null}, options={"expose"=true})
     * @Template()
     * @ParamConverter("project", class="AABAccountBundle:Project")
     * @param Request $request
     * @param Project $project
     *
     * @return array
     */
    public function projectAction( Request $request, Project $project )
    {
        $form = $this->createForm( ProjectType::class, $project );

        if ( $request->isMethod( 'post' ) ) {
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $project = $form->getData();
                $this->persistAndFlush( $project );
                $this->setFlashSuccess( 'Project Updated' );

                return $this->redirect( $this->generateUrl( 'aab_admin_project', array( 'id' => $project->getId() ) ) );
            } else {
                $this->setFlashError( 'Could not update project' );
            }
        }

        $newAccountUrl = $this->generateUrl( 'api_aab_new_project_account', array( 'project' => $project->getId(), '_format' => 'html' ) );
        $tableActions  = array(
            array(
                'item'       => '<button type="button" data-action="create" class="btn btn-success" data-ready-url="' . $newAccountUrl . '" onclick="createNewAccount($(this))"><i class="fa fa-plus-circle"></i> Create New Project</button>',
                'action'     => 'create',
                'dataItem'   => '<button type="button" class="btn btn-success" data-action="create" data-ready-url="' . $newAccountUrl . '" onclick="createNewAccount($(this))" onclick="editAccount($(this))" ><i class="fa fa-plus-circle"></i> Create New Project</button>',
                'resource'   => 'Account',
                'type'       => 'button',
                'url'        => 'api_aab_new_project_account',
                'urlReady'   => $newAccountUrl,
                'buttonType' => 'success',
                'iconClass'  => 'fa fa-plus-circle',
                'target'     => '_self',
                'text'       => 'Create New Account',
                'internal'   => TRUE
            )
        );

        return array(
            'form'            => $form->createView(),
            'project'         => $project,
            'emptyAccount'    => new Account(),
            'dtHtmlOverrides' => array(
                'title'        => 'Project Accounts',
                'tableActions' => $tableActions
            ),
            'dtJsOverrides'   => array(
                'dtOptions' => array(
                    'ajax' => array(
                        'url' => $this->generateUrl( 'api_aab_post_project_accounts_datatables', array( 'project' => $project->getId() ) )
                    )
                ),
                'options'   => array(
                    'tableActions' => $tableActions
                )
            )
        );
    }

    /**
     * @Route("/account/{id}", name="aab_admin_account", defaults={"id"=null}, options={"expose"=true})
     * @Template
     * @ParamConverter("account", class="AABAccountBundle:Account")
     *
     * @param Request $request
     * @param Account $account
     *
     * @return array
     */
    public function accountAction( Request $request, Account $account )
    {
        $form = $this->createForm( AccountType::class, $account, array( 'action' => $this->generateUrl('aab_admin_account', array( 'id' => $account->getId() ))) );

        if ( $request->isMethod( 'post' ) ) {
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $account = $form->getData();
                $this->persistAndFlush( $account );
                $this->setFlashSuccess( 'Account Updated' );

                return $this->redirect(
                    $this->generateUrl( 'aab_admin_account', array( 'id' => $account->getId() ) )
                );
            } else {
                $this->setFlashError( 'Could not update Account' );
            }
        }

        $emptySource    = new Source();
        $emptyRssSource = new RssSource();
        $emptyLander    = new Lander();

        $allSources = $this->getRepo( 'AABAccountBundle:Source' )->findAll();
        $sources    = array();
        $rssSources = array();
        foreach ( $allSources as $source ) {
            if ( $source instanceof RssSource ) {
                $rssSources[] = $source;
            } else {
                $sources[] = $source;
            }
        }

        $landers = $this->getRepo( 'AABAccountBundle:Lander' )->findAll();

        return array(
            'form'           => $form->createView(),
            'account'        => $account,
            'emptySource'    => $emptySource,
            'emptyRssSource' => $emptyRssSource,
            'emptyLander'    => $emptyLander,
            'sources'        => $sources,
            'rssSources'     => $rssSources,
            'landers'        => $landers
        );
    }

    /**
     * @Route("/sources", name="aab_admin_sources")
     * @Template()
     */
    public function sourcesAction()
    {
        return array(
            'source'    => new Source(),
            'rssSource' => new RssSource()
        );
    }

    /**
     * @Route("/landers", name="aab_admin_landers")
     * @Template()
     */
    public function landersAction()
    {
        return array(
            'entity' => new Lander()
        );
    }
}
