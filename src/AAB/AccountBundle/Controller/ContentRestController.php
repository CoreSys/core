<?php

namespace AAB\AccountBundle\Controller;

use AAB\AccountBundle\Entity\Content;
use AAB\AccountBundle\Form\ContentType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
* Content Controller
* @Rest\RouteResource("Content")
* @Rest\NamePrefix("api_aab_")
*/
class ContentRestController extends BaseRestController
{
    /**
    * Datatables Content entity
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Return the datatables response for Content",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is an internal server error"
    *       }
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @return Response
        */
    public function getDatatablesAction(Request $request)
{
    $repo = $this->getRepo( 'AABAccountBundle:Content' );
    return $this->processDatatables( $request, $repo );
}
    /**
    * Datatables Content entity
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Return the datatables response for Content",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is an internal server error"
    *       }
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @return Response
        */
    public function postDatatablesAction(Request $request)
{
    $repo = $this->getRepo( 'AABAccountBundle:Content' );
    return $this->processDatatables( $request, $repo );
}
   /**
    * Get a Content entity
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Get a single Content entity",
    *   statusCodes={
    *       200="Returned when successful",
    *       404="Returned when Content not found",
    *       500="Returned when there is a server error"
    *   },
    *   parameters={
    *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for Content" }
    *   },
    *   output="AAB\AccountBundle/entity/Content"
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @return Response
        */
    public function getAction(Content $entity)
    {
        return $entity;
    }
    /**
    * Get all Content entities
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @ApiDoc(
    *       resource=true,
    *       desription="Return all Content(s)",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is a sever error"
    *       },
    *       output="ArrayCollection<AAB\AccountBundle\Entity\Content>",
    *       requirements={
    *           {
    *               "name"="limit",
    *               "dataType"="integer",
    *               "requirement"="\d+",
    *               "description"="How many Content(s) to return"
    *           }
    *       },
    *       parameters={
    *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many Content(s) to return" },
    *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
    *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
    *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
    *       }
    *   )
    *
    * @param ParamFetcherInterface $paramFetcher
    * @return Response
    *
    * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
    * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
    * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
    * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
        */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
{
    try {
        $offset = $paramFetcher->get( 'offset' );
        $limit = $paramFetcher->get( 'limit' );
        $order_by = $paramFetcher->get( 'order_by' );
        $filters = !is_null( $paramFetcher->get( 'filters' ) )
            ? $paramFetcher->get( 'filters' )
            : array();

        $entities = $this->getRepository( 'AABAccountBundle:Content' )
            ->findBy( $filters, $order_by, $limit, $offset );

        if( $entities ) {
            return $entities;
        }

        return FOSView::create('Not Found', Codes::HTTP_NO_CONTENT);
    } catch( \Exception $e ) {
        return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
}
/**
    * Create a new Content
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Create a new Content",
    *   statusCodes={
    *       201="Returned when successful",
    *       400="Returned when there is an error",
    *       500="Returned where there is a server error"
    *   },
    *   input="AAB\AccountBundle/Form/ContentType",
    *   output="AAB\AccountBundle/Entity/Content"
    * )
    *
    * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @return Response
    */
    public function postAction(Request $request)
{
    $entity = new Content();
    $form = $this->createForm( ContentType::class, $entity, array( 'method' => $request->getMethod()));
    $this->removeExtraFields( $request, $form );
    $form->handleRequest( $request );

    if( $form->isValid() ) {
        $entity = $form->getData();
        $this->persistAndFlush( $entity );

        return $entity;
    }
    return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
}
/**
    * Update a(n) Content entity
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Update a(n) Content",
    *   statusCodes={
    *       201="Returned when successful",
    *       400="Returned when there is an error",
    *       500="Returned where there is a server error"
    *   },
    *   input="AAB\AccountBundle/Form/ContentType",
    *   output="AAB\AccountBundle/Entity/Content"
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param $entity
    * @return Response
    */
    public function putAction(Request $request, Content $entity)
{
    try {
        $request->setMethod( 'PATCH' ); /* treat all puts as patch */
        $form = $this->createForm(ContentType::class, $entity, array( 'method' => $request->getMethod()));
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );
        if( $form->isValid() ) {
            $this->persistAndFlush( $entity );
            return $entity;
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    } catch( \Exception $e ) {
        return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
}
/**
    * Patch a(n) Content entity
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Update a(n) Content",
    *   statusCodes={
    *       201="Returned when successful",
    *       400="Returned when there is an error",
    *       500="Returned where there is a server error"
    *   },
    *   input="AAB\AccountBundle/Form/ContentType",
    *   output="AAB\AccountBundle/Entity/Content"
    * )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param Content $entity
    * @return Response
    */
    public function patchAction(Request $request, Content $entity)
{
    return $this->putAction( $request, $entity );
}
/**
    * Delete a(n) Content
    *
    * @ApiDoc(
    *   resource=true,
    *   description="Delete a(n) Content",
    *   statusCodes={
    *       204="Returned when successful",
    *       404="Returned when Content not found",
    *       500="Returned when there is a server error"
    *   }
    * )
    *
    * @Rest\View(statusCode=204)
    *
    * @param Request $request
    * @param Content $entity
    * @return Response
    */
    public function deleteAction(Request $request, Content $entity)
{
    try {
        $this->removeAndFlush( $entity );

        return null;
    } catch( \Exception $e ) {
        return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
}
    /**
    * Get the form for a new Content
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Get a new Content form to create a new Content",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           500="Returned when there is an internal server error"
    *       }
    *   )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @return Response
        */
    public function newAction()
{
    $entity = new Content();
    $form = $this->createForm(ContentType::class, $entity, array('method' => 'POST'));
    $view = FOSView::create( array( 'form' => $form->createView() ) );
    $view->setTemplate( "AABAccountBundle:ContentRest:new.html.twig" )
        ->setTemplateVar( 'form' );

    return $this->handleView( $view );
}
    /**
    * Get the form for a(n) Content to edit
    *
    * @ApiDoc(
    *       resource=true,
    *       description="Get a the form for Content to edit",
    *       statusCodes={
    *           200="Returned when successful",
    *           400="Returned when there is an error",
    *           404="Returned when Content not found",
    *           500="Returned when there is an internal server error"
    *       }
    *   )
    *
    * @Rest\View(serializerEnableMaxDepthChecks=true)
    *
    * @return Response
        */
    public function editAction(Content $entity)
{
    $form = $this->createForm(ContentType::class, $entity, array('method' => 'POST'));
    $view = FOSView::create( array( 'form' => $form->createView() ) );
    $view->setTemplate( "AABAccountBundle:ContentRest:edit.html.twig" )
    ->setTemplateVar( 'form' );

    return $this->handleView( $view );
}
}