<?php

namespace AAB\AccountBundle\Controller;

use AAB\AccountBundle\Entity\Category;
use AAB\AccountBundle\Form\CategoryType;

use CoreSys\CoreBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Category Controller
 * @Rest\RouteResource("Category")
 * @Rest\NamePrefix("api_aab_")
 */
class CategoryRestController extends BaseRestController
{

    /**
     * Datatables Category entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Category",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'AABAccountBundle:Category' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables Category entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Category",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'AABAccountBundle:Category' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Get a Category entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single Category entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when Category not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for Category" }
     *   },
     *   output="AAB\AccountBundle/entity/Category"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( Category $entity )
    {
        return $entity;
    }

    /**
     * Get all Category entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all Category(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<AAB\AccountBundle\Entity\Category>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many Category(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many Category(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'AABAccountBundle:Category' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new Category
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new Category",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="AAB\AccountBundle/Form/CategoryType",
     *   output="AAB\AccountBundle/Entity/Category"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        $entity = new Category();
        $form   = $this->createForm( CategoryType::class, $entity, array( 'method' => $request->getMethod() ) );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $this->persistAndFlush( $entity );

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Update a(n) Category entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Category",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="AAB\AccountBundle/Form/CategoryType",
     *   output="AAB\AccountBundle/Entity/Category"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, Category $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $form = $this->createForm( CategoryType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $this->persistAndFlush( $entity );

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Patch a(n) Category entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Category",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="AAB\AccountBundle/Form/CategoryType",
     *   output="AAB\AccountBundle/Entity/Category"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request  $request
     * @param Category $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, Category $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) Category
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) Category",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when Category not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request  $request
     * @param Category $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, Category $entity )
    {
        try {
            $this->removeAndFlush( $entity );

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new Category
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new Category form to create a new Category",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity = new Category();
        $form   = $this->createForm( CategoryType::class, $entity, array( 'method' => 'POST' ) );
        $view   = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "AABAccountBundle:CategoryRest:new.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) Category to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for Category to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when Category not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( Category $entity )
    {
        $form = $this->createForm( CategoryType::class, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "AABAccountBundle:CategoryRest:edit.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }
}