<?php

namespace AAB\AccountBundle\Controller;

use AAB\AccountBundle\Entity\Account;
use AAB\AccountBundle\Entity\Lander;
use AAB\AccountBundle\Entity\Project;
use AAB\AccountBundle\Entity\RssSource;
use AAB\AccountBundle\Entity\Source;
use AAB\AccountBundle\Form\AccountType;

use AAB\AccountBundle\Form\RssSourceType;
use AAB\AccountBundle\Form\SourceType;
use CoreSys\CoreBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Account Controller
 * @Rest\RouteResource("Account")
 * @Rest\NamePrefix("api_aab_")
 */
class AccountRestController extends BaseRestController
{

    /**
     * Datatables Account entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Account",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'AABAccountBundle:Account' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables Account entity
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Return the datatables response for Account",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDatatablesAction( Request $request )
    {
        $repo = $this->getRepo( 'AABAccountBundle:Account' );

        return $this->processDatatables( $request, $repo );
    }

    /**
     * Datatables Account Sources Entity
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Return the datatables response for Account Sources",
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Returned when there is an error",
     *          500="Returned when there is an internal server error"
     *     }
     * )
     *
     * @Rest\view(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param Account $account
     *
     * @return Response
     */
    public function postSourcesDatatablesAction( Request $request, Account $account )
    {
        $repo = $this->getRepo( 'AABAccountBundle:Source' );

        $this->processDatatablesWithRequirements( $request, $repo, array( 'account' => $account ) );
    }

    /**
     * Toggle Account
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Toggle the active state for a account",
     *     statusCodes={
     *          200="Returned when successful",
     *          404="Returned when Category not found"
     *      }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     * @param Request $request
     * @param Project $entity
     *
     * @return Response
     */
    public function toggleAction( Request $request, Project $entity )
    {
        $active = !$entity->getActive();
        $entity->setActive( $active );

        $this->persistAndFlush( $entity );

        return $entity;
    }

    /**
     * Get a Account entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Get a single Account entity",
     *   statusCodes={
     *       200="Returned when successful",
     *       404="Returned when Account not found",
     *       500="Returned when there is a server error"
     *   },
     *   parameters={
     *       {"name"="entity", "required"=true, "dataType"="integer", "description"="The ID for Account" }
     *   },
     *   output="AAB\AccountBundle/entity/Account"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function getAction( Account $entity )
    {
        return $entity;
    }

    /**
     * Get all Account entities
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *       resource=true,
     *       desription="Return all Account(s)",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is a sever error"
     *       },
     *       output="ArrayCollection<AAB\AccountBundle\Entity\Account>",
     *       requirements={
     *           {
     *               "name"="limit",
     *               "dataType"="integer",
     *               "requirement"="\d+",
     *               "description"="How many Account(s) to return"
     *           }
     *       },
     *       parameters={
     *           {"name"="limit", "dataType"="integer", "required"=true, "description"="How many Account(s) to return" },
     *           {"name"="offset", "dataType"="integer", "required"=false, "description"="Offset from which to start listing" },
     *           {"name"="order_by", "dataType"="array", "required"=false, "description"="Order by fields. Must be an array: &order_by[name]=ASC&order_by[description]=DESC" },
     *           {"name"="filters", "dataType"="array", "required"=false, "description"="Filter by fields. Must be an array: &filters[id]=3&filters[name]=123" }
     *       }
     *   )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param mixed                 $project
     *
     * @return Response
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction( ParamFetcherInterface $paramFetcher, Project $project = NULL )
    {
        try {
            $offset   = $paramFetcher->get( 'offset' );
            $limit    = $paramFetcher->get( 'limit' );
            $order_by = $paramFetcher->get( 'order_by' );
            $filters  = !is_null( $paramFetcher->get( 'filters' ) )
                ? $paramFetcher->get( 'filters' )
                : array();

            $entities = $this->getRepository( 'AABAccountBundle:Account' )
                             ->findBy( $filters, $order_by, $limit, $offset );

            if ( $entities ) {
                return $entities;
            }

            return FOSView::create( 'Not Found', Codes::HTTP_NO_CONTENT );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Create a new Account
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Create a new Account",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="AAB\AccountBundle/Form/AccountType",
     *   output="AAB\AccountBundle/Entity/Account"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postAction( Request $request )
    {
        $entity = new Account();
        $form   = $this->createForm( AccountType::class, $entity, array( 'method' => $request->getMethod() ) );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $this->persistAndFlush( $entity );

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Create a new Source for an Account
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Create a new Source for an Account",
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Returned when there is an error",
     *          500="Returned when there is an internal server error"
     *     },
     *     input="AAB\AccountBundle\Form\SourceType",
     *     output="AAB\AccountBundle\Entity\Account"
     * )
     *
     * @Rest\View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param Account $account
     *
     * @return Response
     */
    public function postSourceAction( Request $request, Account $account )
    {
        $entity = new Source();
        $form   = $this->createForm( SourceType::class, $entity );
        $this->removeExtraFields( $request, $form );
        $form->handleRequest( $request );

        if ( $form->isValid() ) {
            $entity = $form->getData();
            $entity->setAccount( $account );

            $this->persistAndFlush( $entity );

            return $entity;
        }

        return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
    }

    /**
     * Update a(n) Account entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Account",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="AAB\AccountBundle/Form/AccountType",
     *   output="AAB\AccountBundle/Entity/Account"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param         $entity
     *
     * @return Response
     */
    public function putAction( Request $request, Account $entity )
    {
        try {
            $request->setMethod( 'PATCH' ); /* treat all puts as patch */
            $form = $this->createForm( AccountType::class, $entity, array( 'method' => $request->getMethod() ) );
            $this->removeExtraFields( $request, $form );
            $form->handleRequest( $request );
            if ( $form->isValid() ) {
                $this->persistAndFlush( $entity );

                return $entity;
            }

            return FOSView::create( array( 'errors' => $form->getErrors() ), Codes::HTTP_INTERNAL_SERVER_ERROR );
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Patch a(n) Account entity
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Update a(n) Account",
     *   statusCodes={
     *       201="Returned when successful",
     *       400="Returned when there is an error",
     *       500="Returned where there is a server error"
     *   },
     *   input="AAB\AccountBundle/Form/AccountType",
     *   output="AAB\AccountBundle/Entity/Account"
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param Account $entity
     *
     * @return Response
     */
    public function patchAction( Request $request, Account $entity )
    {
        return $this->putAction( $request, $entity );
    }

    /**
     * Delete a(n) Account
     *
     * @ApiDoc(
     *   resource=true,
     *   description="Delete a(n) Account",
     *   statusCodes={
     *       204="Returned when successful",
     *       404="Returned when Account not found",
     *       500="Returned when there is a server error"
     *   }
     * )
     *
     * @Rest\View(statusCode=204)
     *
     * @param Request $request
     * @param Account $entity
     *
     * @return Response
     */
    public function deleteAction( Request $request, Account $entity )
    {
        try {
            $this->removeAndFlush( $entity );

            return NULL;
        } catch ( \Exception $e ) {
            return FOSView::create( $e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR );
        }
    }

    /**
     * Get the form for a new Account
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new Account form to create a new Account",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function newAction()
    {
        $entity = new Account();
        $form   = $this->createForm( AccountType::class, $entity, array( 'method' => 'POST' ) );
        $view   = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "AABAccountBundle:AccountRest:new.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a new Source under an Account
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a new Source form",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Account $account
     *
     * @return Response
     */
    public function newSourceAction( Account $account )
    {
        $entity    = new Source();
        $rssEntity = new RssSource();
        $actionUrl = $this->generateUrl( 'api_aab_post_account_source', array( 'account' => $account->getId() ) );

        $form    = $this->createForm( SourceType::class, $entity, array( 'method' => 'POST', 'action' => $actionUrl ) );
        $rssForm = $this->createForm( RssSourceType::class, $rssEntity, array( 'method' => 'POST', 'action' => $actionUrl ) );

        $forms = array(
            'source' => $form->createView(),
            'rss'    => $rssForm->createView()
        );

        $view = FOSView::create( array( 'forms' => $forms ) );
        $view->setTemplate( "AABAccountBundle:SourceRest:newSources.html.twig" )
             ->setTemplateVar( 'forms' );

        return $this->handleView( $view );
    }

    /**
     * Get the form for a(n) Account to edit
     *
     * @ApiDoc(
     *       resource=true,
     *       description="Get a the form for Account to edit",
     *       statusCodes={
     *           200="Returned when successful",
     *           400="Returned when there is an error",
     *           404="Returned when Account not found",
     *           500="Returned when there is an internal server error"
     *       }
     *   )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     */
    public function editAction( Account $entity )
    {
        $form = $this->createForm( AccountType::class, $entity, array( 'method' => 'POST' ) );
        $view = FOSView::create( array( 'form' => $form->createView() ) );
        $view->setTemplate( "AABAccountBundle:AccountRest:edit.html.twig" )
             ->setTemplateVar( 'form' );

        return $this->handleView( $view );
    }

    /**
     * Link a source to an account
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Link a source to an account",
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Returned when there is an error",
     *          404="Returned when account or source is not found",
     *          500="Returned when there is an internal server errror"
     *     }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Account $account
     * @param Source  $source
     *
     * @return Response
     */
    public function linkSourceAction( Account $account, Source $source )
    {
        $account->addSource( $source );
        $this->persist( $account );
        $this->persist( $source );
        $this->flush();

        return $source;
    }

    /**
     * Unlink a source to an account
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Unlink a source to an account",
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Returned when there is an error",
     *          404="Returned when account or source is not found",
     *          500="Returned when there is an internal server errror"
     *     }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Account $account
     * @param Source  $source
     *
     * @return Response
     */
    public function unlinkSourceAction( Account $account, Source $source )
    {
        $account->removeSource( $source );
        $this->persist( $account );
        $this->persist( $source );
        $this->flush();

        return $source;
    }

    /**
     * Link a lander to an account
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Link a lander to an account",
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Returned when there is an error",
     *          404="Returned when account or lander is not found",
     *          500="Returned when there is an internal server errror"
     *     }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Account $account
     * @param Lander  $lander
     *
     * @return Response
     */
    public function linkLanderAction( Account $account, Lander $lander )
    {
        $account->addLander( $lander );
        $this->persist( $account );
        $this->persist( $lander );
        $this->flush();

        return $lander;
    }

    /**
     * Unlink a lander to an account
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Unlink a lander to an account",
     *     statusCodes={
     *          200="Returned when successful",
     *          400="Returned when there is an error",
     *          404="Returned when account or lander is not found",
     *          500="Returned when there is an internal server errror"
     *     }
     * )
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Account $account
     * @param Lander  $lander
     *
     * @return Response
     */
    public function unlinkLanderAction( Account $account, Lander $lander )
    {
        $account->removeLander( $lander );
        $this->persist( $account );
        $this->persist( $lander );
        $this->flush();

        return $lander;
    }
}