<?php

namespace CoreSys\CoreBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class ArrayToIdTransformer
 * @package CoreSys\CoreBundle\Form\DataTransformer
 */
class ArrayToIdTransformer implements DataTransformerInterface
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var
     */
    private $class;

    /**
     * ArrayToIdTransformer constructor.
     *
     * @param EntityManager $em
     * @param               $class
     */
    public function __construct( EntityManager $em, $class )
    {
        $this->em    = $em;
        $this->class = $class;
    }

    /**
     * @param mixed $data
     *
     * @return mixed|null
     */
    public function reverseTransform( $data )
    {
        if ( !$data ) {
            return NULL;
        }

        if ( is_scalar( $data ) ) {
            return $data;
        }

        if ( is_array( $data ) && isset( $data[ 'id' ] ) ) {
            return $data[ 'id' ];
        }

        return NULL;
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function transform( $data )
    {
        return $data;
    }

    /**
     * Get em
     *
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * Set Em
     *
     * @param EntityManager $em
     *
     * @return ArrayToIdTransformer
     */
    public function setEm( $em = NULL )
    {
        $this->em = $em;

        return $this;
    }

    /**
     * Get class
     *
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set Class
     *
     * @param mixed $class
     *
     * @return ArrayToIdTransformer
     */
    public function setClass( $class = NULL )
    {
        $this->class = $class;

        return $this;
    }
}