<?php

namespace CoreSys\CoreBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CoreSys\CoreBundle\Form\DataTransformer\ArrayToIdTransformer;
use JMS\DiExtraBundle\Annotation as DI;


/**
 * Class CoreSysEntityType
 * @package CoreSys\CoreBundle\Form\Type
 * @DI\Service("core_sys_core.form.core_sys_entity")
 * @DI\Tag("form.type", attributes={"alias"="coresys_entity"})
 */
class CoreSysEntityType extends EntityType
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * CoreSysEntityType constructor.
     *
     * @param EntityManager $em
     * @DI\InjectParams({"em"=@DI\Inject("doctrine.orm.entity_manager")})
     */
    public function __construct( EntityManager $em )
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $view_transformer = new ArrayToIdTransformer( $this->em, NULL );
        $builder->addViewTransformer( $view_transformer );
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions( OptionsResolverInterface $resolver )
    {
        $resolver->setDefaults(
            array(
                'invalid_message' => 'The selected entity does not exist.',
                'csrf_'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'core_sys_core_entity';
    }
}