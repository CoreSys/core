<?php

namespace CoreSys\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigurationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'siteName', TextType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Site Name' ) ) )
            ->add( 'siteSlogan', TextType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Site Slogan' ) ) )
            ->add( 'siteTitle', TextType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Site Title' ) ) )
            ->add( 'companyName', TextType::class, array( 'required' => TRUE, 'attr' => array( 'placeholder' => 'Company Name' ) ) )
            ->add( 'metaKeywords', CollectionType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Keywords' ) ) )
            ->add( 'metaDescription', TextareaType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Description' ) ) )
            ->add( 'metaAuthor', TextType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Author' ) ) )
            ->add( 'adminEmail', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Admin Email' ) ) )
            ->add( 'supportEmail', EmailType::class, array( 'required' => FALSE, 'attr' => array( 'placeholder' => 'Support Email' ) ) )
            ->add( 'socialLogin', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'rel' => 'socialLogin' ) ) )
            ->add( 'facebookLogin', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'rel' => 'facebookLogin' ) ) )
            ->add( 'twitterLogin', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'rel' => 'twitterLogin' ) ) )
            ->add( 'googleLogin', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'rel' => 'googleLogin' ) ) )
            ->add( 'instagramLogin', CheckboxType::class, array( 'required' => FALSE, 'attr' => array( 'rel' => 'instagramLogin' ) ) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( array(
                                    'data_class' => 'CoreSys\CoreBundle\Entity\Configuration'
                                ) );
    }
}
