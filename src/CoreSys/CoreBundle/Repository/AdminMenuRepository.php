<?php

namespace CoreSys\CoreBundle\Repository;

/**
 * AdminMenuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
/**
 * Class AdminMenuRepository
 * @package CoreSys\CoreBundle\Repository
 */
class AdminMenuRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param null $active
     *
     * @return array
     */
    public function findAll( $active = NULL )
    {
        $q = $this->createQueryBuilder( 'm' )
                  ->orderBy( 'm.position', 'asc' );

        if ( $active !== NULL ) {
            $q->where( 'm.active = :active' )
              ->setParameter( 'active', !!$active );
        }

        return $q->getQuery()->getResult();
    }

    /**
     * @param null $active
     *
     * @return mixed
     */
    public function getMenusCount( $active = NULL )
    {
        $q = $this->createQueryBuilder( 'm' )
                  ->select( 'COUNT(m.id)' );

        if ( $active !== NULL ) {
            $q->where( 'm.active = :active' )
              ->setParameter( 'active', !!$active );
        }

        return $q->getQuery()->getSingleScalarResult();
    }
}
