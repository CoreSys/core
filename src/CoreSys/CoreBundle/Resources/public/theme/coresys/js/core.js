;(function ( $ ) {
    "use strict";

    var CoreSysCoreTheme = function () {};

    CoreSysCoreTheme.prototype.init = function () {
        this.initHeaderMenu();
        this.initWaves();
        this.initPostAjax();
        this.initTooltips();
        this.loadBootstrapSwitches();
        this.loadConfirmations();
    };

    CoreSysCoreTheme.prototype.loadConfirmations = function () {
        setTimeout( function () {
            $( '[data-toggle="confirmation"],[data-confirmation]' ).not( '[data-confirmed]' ).each(
                function () {
                    var $el = $( this );
                    $el.confirmation( {
                        placement:  'left',
                        btnOkClass: "btn btn-sm btn-primary",
                        btnOkLabel: "OK"
                    } );
                    $el.attr( 'data-confirmed', 1 );
                }
            )
        }, 500 );
    }

    CoreSysCoreTheme.prototype.initTooltips = function () {
        $( '[data-toggle="tooltip"]' ).not( '[data-tooltip-loaded]' ).each( function () {
            $( this ).tooltip();
            $( this ).attr( 'data-tooltip-loaded', 1 );
        } );
    };

    CoreSysCoreTheme.prototype.initHeaderMenu = function () {
        $( 'body' ).on( 'click', function ( e ) {
            var $this  = $( e.target ),
                $omits = $( '.navbar-toggler, .nav-container' );

            if ( $omits.has( $this ).length === 0 && !$omits.is( $this ) ) {
                $( '.nav-container.open' ).removeClass( 'open' );
            }
        } );
    };

    CoreSysCoreTheme.prototype.initPostAjax = function () {
        var self = this;
        $( document ).ajaxComplete( function ( event, xhr ) {
            console.log( 'AJAX COMPLETE', event, xhr );
            var contents = xhr.responseText;
            self.postAjaxLoadBootstrapSwitches( contents );
            self.initTabs();
            self.removeInitialLoading();
            self.initTooltips();
            self.loadConfirmations();
        } );
    };

    CoreSysCoreTheme.prototype.postAjaxLoadBootstrapSwitches = function ( content ) {
        var self = this;
        setTimeout( function () {
            if ( $.fn.bootstrapSwitch ) {
                if ( ( content + '' ).indexOf( 'type="checkbox"' ) > 0 ) {
                    setTimeout( function () {
                        self.loadBootstrapSwitches();
                    }, 250 );
                }
            }
        }, 250 );
    };

    CoreSysCoreTheme.prototype.loadBootstrapSwitches = function () {
        $( 'input[type=checkbox]' ).bootstrapSwitch( {
            onText:  '<i class="fa fa-check text-success"></i>',
            offText: '<i class="fa fa-close text-danger"></i>'
        } );
    };

    CoreSysCoreTheme.prototype.removeInitialLoading = function () {
        $( '.initialLoading' ).removeClass( 'initialLoading' );
    };

    CoreSysCoreTheme.prototype.initTabs = function () {
        $( 'ul.nav-tabs a[data-toggle="tab"]' ).off( 'click' ).on( 'click', function ( e ) {
            e.preventDefault();
            $( this ).tab( 'show' );
        } );
    };

    CoreSysCoreTheme.prototype.initWaves = function () {
        if ( Waves ) {
            Waves.attach( '.nav.nav-wave li.nav-item', ['waves-button'] );
            Waves.attach( 'button:not(.no-waves), .nav.nav-wave > li', ['waves-button', 'waves-light'] );
            Waves.init();
        }
    };

    $.CoreSysCoreTheme = new CoreSysCoreTheme();
    $.CoreSysCoreTheme.init();
})( jQuery );