<?php

namespace CoreSys\CoreBundle\Entity;

use CoreSys\CoreBundle\Controller\BaseController;
use Doctrine\ORM\Mapping as ORM;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\Annotation as JMS;

/**
 * Class BaseEntity
 * @package CoreSys\CoreBundle\Entity
 * @JMS\ExclusionPolicy("all")
 */
class BaseEntity
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"Default", "admin"})
     */
    protected $id;

    /**
     * @var BaseController
     */
    private $baseController;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return intval( $this->id );
    }

    /**
     * Get baseController
     *
     * @return BaseController
     */
    public function getBaseController()
    {
        return $this->baseController;
    }

    /**
     * @param     $numeric
     * @param int $min
     * @param int $max
     *
     * @return float
     */
    public function rangeCheck( $numeric, $min = 0, $max = 9999999999 )
    {
        $numeric = floatval( $numeric );
        if ( $numeric <= $min ) {
            return floatval( $min );
        } else if ( $numeric >= $max ) {
            return floatval( $max );
        }

        return $numeric;
    }
}