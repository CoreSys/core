<?php

namespace CoreSys\CoreBundle\Twig;

use CoreSys\CoreBundle\Entity\Note;
use CoreSys\CoreBundle\Form\NoteType;
use CoreSys\CoreBundle\Form\TodoType;
use CoreSys\CoreBundle\Manager\NoteManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class NotesTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.notes", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class NotesTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_notes';

    /**
     * @var NoteManager
     */
    protected $manager;

    /**
     * @return NoteManager
     */
    public function getManager()
    {
        if ( !empty( $this->manager ) ) {
            return $this->manager;
        }

        return $this->manager = $this->get( 'core_sys_core.manager.notes' );
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_getTodos', array( $this, 'getTodos' ) ),
            new \Twig_SimpleFunction( 'cs_getTodoForm', array( $this, 'getTodoForm' ) ),
            new \Twig_SimpleFunction( 'cs_getComments', array( $this, 'getComments' ) ),
            new \Twig_SimpleFunction( 'cs_getNotes', array( $this, 'getNotes' ) ),
        );
    }

    public function getTodoForm()
    {
        $todo = new Note();
        $todo->setType( 'todo' );
        $form = $this->baseController->createForm( TodoType::class, $todo, array( 'method' => 'POST' ) );

        return $form->createView();
    }

    /**
     * @param string $type
     *
     * @return array
     */
    public function getEntities( $type = 'note' )
    {
        switch ( strtolower( trim( $type ) ) ) {
            case 'comments':
                return $this->getManager()->getAllComments();
            case 'todos':
                return $this->getManager()->getAllTodos();
            default:
                return $this->getManager()->getAllNotes();
        }
    }

    /**
     * @return array
     */
    public function getNotes()
    {
        return $this->getEntities( 'note' );
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->getEntites( 'comment' );
    }

    /**
     * @return array
     */
    public function getTodos()
    {
        return $this->getEntities( 'todo' );
    }
}