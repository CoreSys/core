<?php

namespace CoreSys\CoreBundle\Twig;

use CoreSys\CoreBundle\Controller\BaseController;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class BaseTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class BaseTwig extends \Twig_Extension
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_base';

    /**
     * @var BaseController
     */
    protected $baseController;

    /**
     * BaseTwig constructor.
     *
     * @param BaseController $baseController
     * @DI\InjectParams({
     *     "baseController" = @DI\Inject("core_sys_core.controller.base")
     * })
     */
    public function __construct( BaseController $baseController )
    {
        $this->baseController = $baseController;
    }

    /**
     * @return BaseController
     */
    public function getBaseController()
    {
        return $this->baseController;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array();
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call( $name, $arguments )
    {
        if ( is_object( $this->baseController ) && method_exists( $this->baseController, $name ) ) {
            return call_user_func_array( array(
                                             $this->baseController,
                                             $name
                                         ), $arguments );
        }

        throw new \Exception( 'Method ' . $name . ' Not Found' );
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        $requestStack = $this->baseController->get( 'request_stack' );
        $request      = $requestStack->getCurrentRequest();

        return $request;
    }
}