<?php

namespace CoreSys\CoreBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class CommonTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.common", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class CommonTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_common';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_isBool', array( $this, 'isBool' ) ),
            new \Twig_SimpleFunction( 'cs_isString', array( $this, 'isString' ) ),
            new \Twig_SimpleFunction( 'cs_isNumeric', array( $this, 'isNumeric' ) ),
            new \Twig_SimpleFunction( 'cs_isArray', array( $this, 'isArray' ) ),
            new \Twig_SimpleFunction( 'cs_isNull', array( $this, 'isNull' ) ),
            new \Twig_SimpleFunction( 'cs_urlEncode', array( $this, 'urlEncode' ), array( 'is_safe' => array( 'html', 'js' ) ) ),
            new \Twig_SimpleFunction( 'cs_removeLineBreaks', array( $this, 'removeLineBreaks' ) )
        );
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function urlEncode( $url )
    {
        return urlencode( $url );
    }

    /**
     * @param $val
     *
     * @return bool
     */
    public function isNull( $val )
    {
        return $val === NULL;
    }

    /**
     * @param $content
     *
     * @return mixed
     */
    public function removeLineBreaks( $content )
    {
        return preg_replace( '/[\r\n]+/', '', $content );
    }

    /**
     * @param $val
     *
     * @return bool
     */
    public function isBool( $val )
    {
        return $val === TRUE || $val === FALSE;
    }

    /**
     * @param $val
     *
     * @return bool
     */
    public function isString( $val )
    {
        return is_string( $val );
    }

    /**
     * @param $val
     *
     * @return bool
     */
    public function isNumeric( $val )
    {
        return is_numeric( $val );
    }

    /**
     * @param $arr
     *
     * @return bool
     */
    public function isArray( $arr )
    {
        return is_array( $arr );
    }
}

class SessionMessage
{
    private $sessionKey = 'session_message_data';

    private $classes;

    private $icons;

    private $messages;

    public function __construct( array $classes = array(), array $icons = array(), $messages = array() )
    {
        $this->classes   = $classes;
        $this->icons     = $icons;
        $this->messasges = $messages;

        $this->loadSessionData();
    }

    public function addMessage( $type, $message )
    {
        $this->messages[] = array(
            'msg'  => $message,
            'type' => $type
        );
        $this->saveSession();
    }

    public function output()
    {
        $output = array();
        if( is_array( $this->messages ) && count( $this->messages ) ) {
            foreach( $this->messages as $message ) {
                $output[] = '<div class="alert alert-' . $message['type'] . '">' . $message['msg'] . '</div>';
            }
        }
        $this->clearMessages();
        return implode( $output );
    }

    protected function loadSessionData()
    {
        $data = isset( $_SESSION[ $this->sessionKey ] ) ? $_SESSION[ $this->sessionKey ] : NULL;
        if ( !empty( $data ) ) {
            foreach ( $data as $k => $v ) {
                switch ( $k ) {
                    case 'classes':
                        $this->classes = $v;
                        break;
                    case 'icons':
                        $this->icons = $v;
                        break;
                    case 'messages':
                        $this->messages = $v;
                        break;
                }
            }
        }
    }

    protected function saveSession()
    {
        $_SESSION[ $this->sessionKey ] = array(
            'classes'  => $this->classes,
            'icons'    => $this->icons,
            'messages' => $this->messages
        );
    }

    protected function clearSession()
    {
        unset( $_SESSION[ $this->sessionKey ] );
    }

    protected function clearMessages()
    {
        $this->messages = array();
        $this->saveSession();
    }
}