<?php

namespace CoreSys\CoreBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class HelpersTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.helpers", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class HelpersTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_helpers';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_openPanel', array( $this, 'openPanel' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_openCard', array( $this, 'openCard' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_closePanel', array( $this, 'closePanel' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_closeCard', array( $this, 'closeCard' ), array( 'is_safe' => array( 'html' ) ) ),
            new \Twig_SimpleFunction( 'cs_helperAngularIncludes', array( $this, 'getAngularIncludes' ), array( 'is_safe' => array( 'html', 'js' ) ) ),
            new \Twig_SimpleFunction( 'cs_helperAngularModule', array( $this, 'getAngularModule' ), array( 'is_safe' => array( 'html', 'js' ) ) ),
        );
    }

    public function getAngularIncludes( )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:Angular/coreJavascripts.html.twig');
    }

    public function getAngularModule( $appName = 'CSApp', $moduleName = 'CSApp', $dependencies = array( 'ngResource', 'ui.bootstrap', 'ui.sortable', 'ui.select' ) )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:Angular/angularModule.js.twig', array(
            'appName'      => $appName,
            'moduleName'   => $moduleName,
            'dependencies' => is_array( $dependencies ) ? $dependencies : array( $dependencies )
        ) );
    }

    public function openPanel( $type = 'default', $title = NULL )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:openPanel.html.twig', array( 'type' => $type, 'title' => $title ) );
    }

    public function openCard( $type = 'default', $title = NULL, $headerBtns = null )
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:openCard.html.twig', array( 'type' => $type, 'title' => $title, 'headerBtns' => $headerBtns ) );
    }

    public function closePanel()
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:closePanel.html.twig' );
    }

    public function closeCard()
    {
        return $this->renderView( 'CoreSysCoreBundle:Helpers:closeCard.html.twig' );
    }
}