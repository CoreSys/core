<?php

namespace CoreSys\CoreBundle\Twig;

use CoreSys\CoreBundle\Entity\User;
use CoreSys\CoreBundle\Form\AdminUserType;
use CoreSys\CoreBundle\Form\UserType;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class UserTwig
 * @package CoreSys\CoreBundle\Twig
 * @DI\Service("core_sys_core.twig.user", parent="core_sys_core.twig.base")
 * @DI\Tag("twig.extension")
 */
class UserTwig extends BaseTwig
{

    /**
     * @var string
     */
    protected $name = 'core_sys_core_twig_user';

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'cs_getUserForm', array( $this, 'getForm' ) ),
            new \Twig_SimpleFunction( 'cs_getUserRegisterForm', array( $this, 'getRegisterForm' ) ),
            new \Twig_SimpleFunction( 'cs_getUserImage', array( $this, 'getUserImage' ) ),
            new \Twig_SimpleFunction( 'cs_getUserGavatarImage', array( $this, 'getUserGravatarImage' ) ),
            new \Twig_SimpleFunction( 'cs_getUserFacebookImage', array( $this, 'getUserFacebookImage' ) )
        );
    }

    public function getUserImage( User $user, $size = 40 )
    {
        $image = NULL;
        if ( $user->getGravatarUse() ) {
            $image = $this->getUserGravatarImage( $user, $size );
        } else if ( $user->hasSocialdata( 'facebook' ) ) {
            $image = $this->getUserFacebookImage( $user );
        }

        return $image;
    }

    /**
     * @return string
     */
    public function getRegisterForm( $options = array() )
    {
        $form = $this->getForm( FALSE );

        return $this->baseController->renderView( 'CoreSysCoreBundle:UserRest:registerForm.html.twig', array( 'form' => $form->createView() ) );
    }

    /**
     *
     * @return Form
     */
    public function getForm( $admin = FALSE, $options = array() )
    {
        $user = new User();

        if ( $admin ) {
            $admin = $this->baseController->isGranted( 'ROLE_ADMIN' );
        }

        if ( $admin ) {
            return $this->baseController->createForm( AdminUserType::class, $user, $options );
        } else {
            return $this->baseController->createForm( UserType::class, $user, $options );
        }
    }

    public function getUserGravatarImage( User $user, $size = 40 )
    {
        $request = $this->getRequest();
        $baseUrl = $request->getBaseUrl();

        $default = $baseUrl . $this->baseController->get( 'assets.packages' )->getUrl( '/bundle/coresyscore/images/defaultUser.jpg' );

        return $user->getGravatarUrl() . '?' . http_build_query( array(
                                                                     'd' => $default,
                                                                     's' => intval( $size )
                                                                 ) );
    }

    public function getUserFacebookImage( User $user )
    {
        $socialData = $user->getSocialData();
        if ( !empty( $socialData ) ) {
            return $socialData->getFacebookPicture();
        }

        return NULL;
    }
}