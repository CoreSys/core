<?php

namespace CoreSys\CoreBundle\Annotation\Datatables\Renderer;

use CoreSys\CoreBundle\Annotation\Datatables\Renderer;

/**
 * Class UrlRenderer
 * @package CoreSys\CoreBundle\Annotation\Datatables\Renderer
 * @Annotation
 */
class UrlRenderer extends Renderer
{
    /**
     *
     * @var string
     */
    public $value = 'renderUrlColumn';

    /**
     * @var string
     */
    public $template = 'CoreSysCoreBundle:Datatables:Renderer\url.js.twig';

    /**
     * @var array
     */
    public $parameters = array();
}