<?php

namespace CoreSys\CoreBundle\Annotation\Datatables\Renderer;

use CoreSys\CoreBundle\Annotation\Datatables\Renderer;

/**
 * Class BlankRenderer
 * @package CoreSys\CoreBundle\Annotation\Datatables\Renderer
 * @Annotation
 */
class BlankRenderer extends Renderer
{
    /**
     *
     * @var string
     */
    public $value = 'renderBlankColumn';

    /**
     * @var string
     */
    public $template = 'CoreSysCoreBundle:Datatables:Renderer\blank.js.twig';

    /**
     * @var array
     */
    public $parameters = array();
}