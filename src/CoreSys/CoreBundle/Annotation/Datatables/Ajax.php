<?php

namespace CoreSys\CoreBundle\Annotation\Datatables;

/**
 * Class Ajax
 * @package CoreSys\CoreBundle\Annotation\Datatables
 * @Annotation
 */
class Ajax
{

    /**
     * @var bool
     */
    public $value = TRUE;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $type = 'POST';
}