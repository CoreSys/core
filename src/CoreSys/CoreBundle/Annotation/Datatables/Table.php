<?php

namespace CoreSys\CoreBundle\Annotation\Datatables;

/**
 * Class Table
 * @package CoreSys\CoreBundle\Annotation\Datatables
 * @Annotation
 */
class Table
{

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $apiBase = "api_";

    /**
     * @var string
     */
    public $createdRow;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $dom;

    /**
     * @var string
     */
    public $createUrl;

    /**
     * @var string
     */
    public $editUrl;

    /**
     * @var string
     */
    public $deleteUrl;

    /**
     * @var bool
     */
    public $responsive = TRUE;

    /**
     * @var bool
     */
    public $serverSide = FALSE;

    /**
     * @var bool
     */
    public $processing = TRUE;

    /**
     * @var mixed
     */
    public $ajax;

    /**
     * @var array<CoreSys\CoreBundle\Annotation\Datatables\Column>
     */
    public $columns;

    /**
     * @var array<CoreSys\CoreBundle\Annotation\Datatables\ColumnDef>
     */
    public $columnDefs;

    /**
     * @var bool
     */
    public $checkable = TRUE;

    /**
     * @var array<CoreSys\CoreBundle\Annotation\Datatables\Action>
     */
    public $tableActions;

    /**
     * @var array<CoreSys\CoreBundle\Annotation\Datatables\Action>
     */
    public $rowActions;

    /**
     * @var string
     */
    public $resource;

    /**
     * @return array
     */
    public function getOptions()
    {
        $this->resource = $this->value;
        $options        = array( 'resource' => $this->value );
        foreach ( get_class_vars( __CLASS__ ) as $var => $val ) {
            if ( $var !== 'value' ) {
                $value = $this->$var;
                if ( !empty( $value ) ) {
                    $options[ $var ] = $value;
                }
            }
        }

        return $options;
    }
}