<?php

namespace CoreSys\CoreBundle\Annotation\Datatables;


use CoreSys\CoreBundle\Annotation\Datatables\Renderer\RendererInterface;

/**
 * Class Renderer
 * @package CoreSys\CoreBundle\Annotation\Datatables
 * @Annotation
 */
class Renderer implements RendererInterface
{

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $template = '';

    /**
     * @var array
     */
    public $parameters = array();

    /**
     * @var mixed
     */
    public $urlName;

    /**
     * @var array
     */
    public $urlParams = array();

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        $this->parameters           = is_array( $this->parameters ) ? $this->parameters : array();
        $this->parameters[ 'uuid' ] = uniqid( 'cb' );

//        if ( !empty( $this->urlName ) ) {
            $this->parameters[ 'urlName' ]   = $this->urlName;
            $this->parameters[ 'urlParams' ] = $this->urlParams;
            $this->parameters[ 'template' ] = $this->template;
//        }

        return $this->parameters;
    }
}