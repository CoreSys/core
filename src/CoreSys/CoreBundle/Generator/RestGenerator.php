<?php

namespace CoreSys\CoreBundle\Generator;

use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Generate a Rest Controller
 */
class RestGenerator extends Generator
{

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var string
     */
    protected $routePrefix;

    /**
     * @var string
     */
    protected $routeNamePrefix;

    /**
     * @var BundleInterface
     */
    protected $bundle;

    /**
     * @var mixed
     */
    protected $entity;

    /**
     * @var ClassMetadata
     */
    protected $metadata;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var array
     */
    protected $actions;

    /**
     * RestGenerator constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct( Filesystem $filesystem )
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param BundleInterface $bundle
     * @param                 $entity
     * @param ClassMetadata   $metadata
     * @param                 $routePrefix
     * @param                 $forceOverwrite
     */
    public function generate( BundleInterface $bundle, $entity, ClassMetadata $metadata, $routePrefix, $forceOverwrite )
    {
        $this->routePrefix     = $routePrefix;
        $this->routeNamePrefix = str_replace( '/', '_', $routePrefix );
        $this->actions         = array( 'getById', 'getAll', 'post', 'put', 'delete' );

        if ( count( $metadata->identifier ) > 1 ) {
            throw new \RuntimeException( 'The Rest Api Generator does not support entity classes with multiple primary keys.' );
        }

        if ( !in_array( 'id', $metadata->identifier ) ) {
            throw new \RuntimeException( 'The Rest Api Generator expects that the entity object has a primary key field names "id" with a getId() method' );
        }

        $this->entity   = $entity;
        $this->bundle   = $bundle;
        $this->metadata = $metadata;
        $this->setFormat( 'yml' );

        $this->generateControllerClass( $forceOverwrite );
    }

    /**
     *
     */
    public function generateConfiguration()
    {
        if ( !in_array( $this->format, array( 'yml', 'xml', 'php' ) ) ) {
            return;
        }

        $target = sprintf(
            '%s/Resource/config/routing/%s.%s',
            $this->bundle->getPath(),
            strtolower( str_replace( '\\', '_', $this->entity ) ),
            $this->format
        );

        $this->renderFile(
            'rest/config/routing.' . $this->format . '.twig',
            $target,
            array(
                'actions'           => $this->actions,
                'route_prefix'      => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'bundle'            => $this->bundle->getName(),
                'entity'            => $this->entity
            )
        );
    }

    /**
     * @param $format
     */
    public function setFormat( $format )
    {
        switch ( $format ) {
            case 'yml':
            case 'xml':
            case 'php':
            case 'annotation':
                $this->format = $format;
                break;
            default:
                $this->format = 'yml';
                break;
        }
    }

    /**
     * @param $forceOverwrite
     */
    public function generateControllerClass( $forceOverwrite )
    {
        $dir = $this->bundle->getPath();

        $parts           = explode( '\\', $this->entity );
        $entityClass     = array_pop( $parts );
        $entityNamespace = implode( '\\', $parts );

        $target = sprintf(
            '%s/Controller/%s/%sRestController.php',
            $dir,
            str_replace( '\\', '/', $entityNamespace ),
            $entityClass
        );

        if ( !$forceOverwrite && file_exists( $target ) ) {
            throw new \RuntimeException( 'Unable to generate the controller as it already exists.' );
        }

        $this->renderFile(
            'rest/controller.php.twig',
            $target,
            array(
                'actions'           => $this->actions,
                'route_prefix'      => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'bundle'            => $this->bundle->getName(),
                'entity'            => $this->entity,
                'entity_class'      => $entityClass,
                'namespace'         => $this->bundle->getNamespace(),
                'entity_namespace'  => $entityNamespace,
                'templateNamespace' => str_replace( '\\', '', str_replace( '/', '', $this->bundle->getNamespace() ) ),
                'format'            => $this->format
            )
        );
    }

    /**
     *
     */
    protected function generateTestClass()
    {
        $parts           = explode( '\\', $this->entity );
        $entityClass     = array_pop( $part );
        $entityNamespace = implode( '\\', $parts );

        $dir    = $this->bundle->getPath() . '/Tests/Controller';
        $target = $dir . '/' . str_replace( '\\', '/', $entityNamespace ) . '/' . $entityClass . 'RestControllerTest.php';

        $this->renderFile(
            'rest/tests/test.php.twig',
            $target,
            array(
                'actions'           => $this->actions,
                'route_prefix'      => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'bundle'            => $this->bundle,
                'entity'            => $this->entity,
                'entity_class'      => $entityClass,
                'namespace'         => $this->bundle->getNamespace(),
                'entity_namespace'  => $entityNamespace,
                'templateNamespace' => str_replace( '\\', '', str_replace( '/', '', $this->bundle->getNamespace() ) ),
                'format'            => $this->format
            )
        );
    }

}