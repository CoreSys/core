<?php

namespace CoreSys\CoreBundle\Provider;

use CoreSys\CoreBundle\Entity\SocialData;
use CoreSys\CoreBundle\Manager\BaseManager;
use CoreSys\CoreBundle\Manager\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class OAuthUserProvider
 * @package CoreSys\CoreBundle\Provider
 */
class OAuthUserProvider extends BaseClass
{

    /**
     * @var UserManager
     */
    protected $coreSysUserManager;

    /**
     * OAuthUserProvider constructor.
     *
     * @param UserManagerInterface $userManager
     * @param array                $properties
     * @param UserManager          $coreSysUserManager
     */
    public function __construct( UserManagerInterface $userManager, UserManager $coreSysUserManager, array $properties )
    {
        parent::__construct( $userManager, $properties );
        $this->coreSysUserManager = $coreSysUserManager;
    }

    public function connect( UserInterface $user, UserResponseInterface $response )
    {
        $property = $this->getProperty( $response );
        $username = $response->getUsername();
        //on connect - get the access token and the user ID
        $service      = $response->getResourceOwner()->getName();
        $setter       = 'set' . ucfirst( $service );
        $setter_id    = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';
        //we "disconnect" previously connected users
        if ( NULL !== $previousUser = $this->userManager->findUserBy( array( $property => $username ) ) ) {
            $previousUser->$setter_id( NULL );
            $previousUser->$setter_token( NULL );
            $this->userManager->updateUser( $previousUser );
        }
        //we connect current user
        $user->$setter_id( $username );
        $user->$setter_token( $response->getAccessToken() );
        $this->userManager->updateUser( $user );
    }

    public function loadUserByOAuthUserResponse( UserResponseInterface $response )
    {
        try {
            $data     = $response->getResponse();
            $username = $response->getUsername();
            $email    = $response->getEmail() ? $response->getEmail() : $username;
            $type     = str_replace( '_id', '', $this->getProperty( $response ) );
            $user     = $this->locateUser( $data, $response, $username, $type );

            //when the user is registering
            if ( NULL === $user ) {
                $user         = $this->userManager->findUserBy( array( 'email' => $email ) );
                $service      = $response->getResourceOwner()->getName();
                $setter       = 'set' . ucfirst( $service );
                $setter_id    = $setter . 'Id';
                $setter_token = $setter . 'AccessToken';

                if ( NULL === $user ) {
                    // create new user here
                    $user = $this->userManager->createUser();
                    //I have set all requested data with the user's username
                    //modify here with relevant data
                    $user->setUsername( $username );
                    $user->setEmail( $email );
                    $user->setPassword( $username );
                    $user->setEnabled( TRUE );
                }

                $user->$setter_id( $username );
                $user->$setter_token( $response->getAccessToken() );

                if ( !empty( $provider ) ) {
                    $provider   = trim( str_replace( '_id', '', $provider ) );
                    $socialData = $user->getSocialData();
                    if ( empty( $socialData ) ) {
                        $socialData = new SocialData();
                        $socialData->setUser( $user );
                        $method = 'set' . ucwords( $provider ) . 'Data';
                        if ( method_exists( $socialData, $method ) ) {
                            $socialData->$method( $data );
                        }
                    }
                    $user->setSocialData( $socialData );
                }

                $this->userManager->updateUser( $user );

                return $user;
            }
            //if user exists - go with the HWIOAuth way
//        $user        = parent::loadUserByOAuthUserResponse( $response );
            $serviceName = $response->getResourceOwner()->getName();
            $setter      = 'set' . ucfirst( $serviceName ) . 'AccessToken';
            //update access token
            $user->$setter( $response->getAccessToken() );

            $this->updateSocialdata( $user, $type, $data );
        } catch( \Exception $e ) {
            echo $e->getLine() . ':' . $e->getFile() . ':' . $e->getMessage();
            exit;
        }
        return $user;
    }

    public function locateUser( array $data = array(), $response, $username, $type )
    {
        $user = $this->userManager->findUserBy( array( $this->getProperty( $response ) => $username ) );
        if ( empty( $user ) ) {
            $repo            = $this->coreSysUserManager->getBaseController()->getRepo( 'CoreSysCoreBundle:SocialAccount' );
            $data[ 'email' ] = isset( $data[ 'email' ] ) ? $data[ 'email' ] : NULL;
            $account         = NULL;
            switch ( strtolower( trim( $type ) ) ) {
                case 'facebook':
                    $account = $repo->findOneBy( array( 'facebookEmail' => $data[ 'email' ] ) );
                    break;
                case 'twitter':
                    $account = $repo->findOneBy( array( 'twitterEmail' => $data[ 'email' ] ) );
                    if ( empty( $account ) ) {
                        $account = $repo->findOneBy( array( 'twitterScreenName' => $data[ 'screen_name' ] ) );
                    }
                    break;
            }

            if ( !empty( $account ) ) {
                $user = $account->getUser();
            }
        }

        return $user;
    }

    public function updateSocialdata( &$user, $type, $data )
    {
        if ( is_array( $data ) && !empty( $user ) ) {
            $social = $user->getSocialData();
            if ( empty( $social ) ) {
                $social = new SocialData();
                $user->setSocialData( $social );
            }
            $method = 'set' . ucfirst( strtolower( trim( $type ) ) ) . 'Data';
            if ( method_exists( $social, $method ) ) {
                $social->$method( $data );
                $this->coreSysUserManager->getBaseController()->persist( $social );
                $this->coreSysUserManager->getBaseController()->persist( $user );
                $this->coreSysUserManager->getBaseController()->flush();
            }
        }
    }
}