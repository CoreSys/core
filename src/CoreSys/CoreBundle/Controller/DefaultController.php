<?php

namespace CoreSys\CoreBundle\Controller;

use CoreSys\CoreBundle\Entity\Access;
use CoreSys\CoreBundle\Entity\BaseEntity;
use CoreSys\CoreBundle\Entity\Role;
use CoreSys\CoreBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package CoreSys\CoreBundle\Controller
 * @Route("/cs_test", options={"expose"=true})
 */
class DefaultController extends BaseController
{

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/baseManager")
     */
    public function userEntityAction()
    {
        $manager = $this->get( 'core_sys_core.manager.base' );
        echo $manager->testFunction();
        exit;
    }

    /**
     * @Route("/dttableoptions")
     * @Template()
     */
    public function dttableOptionsAction()
    {
        $manager   = $this->get( 'core_sys_core.manager.datatables' );
        $dtOptions = $manager->getDatatablesOptions( new Role() );
        echo '<hr><hr>' . "\n\n";
        var_dump( $dtOptions );
        exit;

        return array();
    }

    /**
     * @Route("/dtRoles")
     * @Template()
     */
    public function dtRolesAction()
    {
        $manager = $this->get( 'core_sys_core.manager.roles' );
        return array( 'role' => new Role() );
    }

    /**
     * @Route("/dtAccess")
     * @Template()
     * @return array
     */
    public function dtAccessAction()
    {
        return array( 'access' => new Access() );
    }
}
