<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;
use PHPImageWorkshop\Core\ImageWorkshopLayer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use GifCreator\GifCreator;
use GifFrameExtractor\GifFrameExtractor;
use CoreSys\CoreBundle\Entity\Image;
use CoreSys\CoreBundle\Entity\ImageSize;
use Doctrine\Common\Collections\ArrayCollection;
use PHPImageWorkshop\ImageWorkshop;

use Flow\Config as FlowConfig;
use Flow\Request as FlowRequest;
use Flow\Basic as FlowBasic;

/**
 * Class ImageSizeManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.image", parent="core_sys_core.manager.base")
 */
class ImageManager extends BaseManager
{

    /**
     * @var EntityRepository
     */
    private $imageRepo;

    /**
     * @var ImageSizeManager
     */
    private $sizeManager;

    /**
     * @var array
     */
    private $folders;

    /**
     * @var mixed
     */
    private $prevSource;

    /**
     * ImageManager constructor.
     *
     * @param BaseController     $baseController
     * @param ContainerInterface $container
     */
    public function __construct( BaseController $baseController, ContainerInterface $container )
    {
        parent::__construct( $baseController, $container );
//        $this->debug = TRUE;
//        echo '<style>fieldset{margin:10px 5px;padding:10px;} legend { font-weight: bold; font-size: 1.5rem; }</style>';
        $this->initializeFolders();
        $this->cleanupImages( 60 );
    }

    public function hasPrevSource()
    {
        return !empty( $this->prevSource );
    }

    public function getPrevSource()
    {
        return $this->prevSource;
    }

    public function updatePreviousSource( \stdClass $src, \stdClass $dst )
    {
        if ( !$dst->crop ) {
            $this->prevSource       = $src;
            $this->prevSource->name = $dst->size->getName();
            $transfer               = array( 'width', 'height', 'folder', 'file' );
            foreach ( $transfer as $key ) {
                $this->prevSource->$key = $dst->$key;
            }
        }
    }

    /**
     * @return ImageSizeManager|object
     */
    public function getImageSizeManager()
    {
        if ( !empty( $this->sizeManager ) ) {
            return $this->sizeManager;
        }

        return $this->sizeManager = $this->getBaseController()->get( 'core_sys_core.manager.image_size' );
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|EntityRepository
     */
    public function getImageRepo()
    {
        if ( !empty( $this->imageRepo ) ) {
            return $this->imageRepo;
        }

        return $this->imageRepo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Image' );
    }

    /**
     * When there is no provided image, simply supply the uploaded file
     *
     * @param UploadedFile $file
     * @param bool         $save
     *
     * @return Image|string
     */
    public function processUploadedFile( UploadedFile $file, $save = TRUE )
    {
        if ( !$file instanceof UploadedFile ) {
            return 'File is not an instance of UploadedFile';
        }

//        echo '<fieldset><legend>Process UploadedFile</legend>';

        $image = new Image();
        // send false for save since we will be saving in this method
        $image = $this->processNewImage( $file, $image, FALSE );
        $this->cleanupImages();

//        echo '</fieldset>';

        if ( $save ) {
            $this->getBaseController()->persist( $image );
            $this->getBaseController()->flush();
        }

        return $image;
    }

    /**
     * Process and uploaded file into a pre-existing Image object
     *
     * @param UploadedFile $file
     * @param Image        $image
     * @param bool         $save
     * @param bool         $createBase64
     *
     * @return Image
     */
    public function processNewImage( UploadedFile $file, Image &$image, $save = TRUE, $createBase64 = FALSE )
    {
        if ( !$file instanceof UploadedFile ) {
            return 'File is not an instance of UploadedFile';
        }

//        echo '<fieldset><legend>Processing Uploaded Image</legend>';

        $image->setData( $file );

        $uploadFolder = $this->getFolder( 'public.upload' );
        $this->log( 'UploadFolder: ' . $uploadFolder );
        $uploadFilename = $image->getFilename();
        $this->log( 'UploadFilename: ' . $uploadFilename );
        $uploadFile = $uploadFolder . DIRECTORY_SEPARATOR . $uploadFilename;
        $this->log( 'UploadFile: ' . $uploadFile );

        $ext      = pathinfo( $uploadFilename, PATHINFO_EXTENSION );
        $ext      = $ext === 'jpeg' ? 'jpg' : $ext;
        $filename = $this->getFilenamePrefix() . '.' . microtime( TRUE ) . '.' . $ext;
        $image->setFilename( $filename );
        $this->log( 'New Filename: ' . $filename );

        if ( !is_file( $uploadFile ) ) {
            $file->move( $uploadFolder, $uploadFile );
            if ( !is_file( $uploadFile ) ) {
                return 'Could not upload image';
            }
        }

        $width = $height = 0;
        list( $width, $height ) = getimagesize( $uploadFile );
        $this->log( 'Dimensions: ' . $width . 'x' . $height );
        $ratio1 = $height / $width;
        $ratio2 = $width / $height;
        $image->setWidth( $width );
        $image->setHeight( $height );
        $image->setRatio( $ratio1 );
        $image->setRatioReverse( $ratio2 );

//        echo '<fieldset><legend>Image Data</legend>';
        $imageData = array( 'filename', 'ext', 'width', 'height', 'ratio', 'ratioReverse', 'filesize', 'mimeType' );
        foreach ( $imageData as $item ) {
            $key    = ucfirst( $item );
            $method = 'get' . $key;
            if ( method_exists( $image, $method ) ) {
                $this->log( $key . ': ' . $image->$method() );
            }
        }
//        echo '</fieldset>';

        $result = $this->moveUploadedFileToDatedFolder( $image, $uploadFile );
        if ( !$result ) {
            $this->log( 'Result: ' . $result );

            return $result;
        }

        $this->createImageSizes( $image );

//        echo '</fieldset>';

        if ( $save ) {
            $this->getBaseController()->persist( $image );
            $this->getBaseController()->flush();
        }

        return $image;
    }

    /**
     * @param $image
     * @param $uploadFile
     */
    public function moveUploadedFileToDatedFolder( Image &$image, $uploadFile )
    {
//        echo '<fieldset><legend>Move and Upload to Dated Folder</legend>';
        $dstFolder = $this->getImageDatedFolder( $image );
        $dstFile   = $dstFolder . DIRECTORY_SEPARATOR . $image->getFilename();
        $this->log( 'Destination Folder: ' . $dstFolder );
        $this->log( 'Destination File: ' . $dstFile );
        try {
            copy( $uploadFile, $dstFile );
            $this->log( 'MOVED' );
        } catch ( \Exception $e ) {
            $this->log( 'Could not move' );

//            echo '</fieldset>';

            return FALSE;
        }

//        echo '</fieldset>';

        return TRUE;
    }

    /**
     * @param Image $image
     */
    public function createImageSizes( Image &$image )
    {
//        echo '<fieldset><legend>Create image Sizes</legend>';
        $sizes = $this->getImageSizeManager()->getAvailableImageSizes( TRUE, TRUE );

        $this->log( 'Sizes: ' . count( $sizes ) );

        foreach ( $sizes as $size ) {
            $this->createImageSize( $image, $size );
        }

//        echo '</fieldset>';
    }

    /**
     * @param Image     $image
     * @param ImageSize $size
     */
    public function createImageSize( Image &$image, ImageSize &$size )
    {
//        echo '<fieldset><legend>Create Image Size: (' . $size->getId() . ') ' . $size->getName()
//             . ' (' . $size->getMaxWidth() . 'x' . $size->getMaxHeight() . ') '
//             . ( $size->getCrop()
//                ? 'Crop: ' . $size->getCropLocation()
//                : 'No crop' ) . '</legend>';
        $src = $this->getImageDatedFolder( $image )
               . DIRECTORY_SEPARATOR . $image->getFilename();
        $this->log( 'Source: ' . $src );
        $this->log( 'Animate: ' . ( $size->getAnimate() ? 'Animated' : 'Non-Animated' ) );
        $this->log( 'Force Format: ' . $size->getForceFormat() );

        if ( intval( $size->getMaxWidth() ) > $image->getWidth() ) {
            $image->removeSize( $size );
            $this->log( 'Invalid size' );

//            echo '</fieldset>';

            return FALSE;
        }

        if ( $this->hasPrevSource() ) {
            $this->log( 'Using Previous Source' );
            $src = $this->getPrevSource();
            $this->log( 'Previous Source from: ' . $src->name );
        } else {
            $src         = new \stdClass();
            $src->folder = $this->getImageDatedFolder( $image );
            $src->file   = $src->folder . DIRECTORY_SEPARATOR . $image->getFilename();
            $src->image  = $image;
            $src->width  = $image->getWidth();
            $src->height = $image->getHeight();
        }

        $this->log( 'SRC Folder: ' . $src->folder );
        $this->log( 'SRC File: ' . $src->file );
        $this->log( 'SRC Width: ' . $src->width );
        $this->log( 'SRC Height: ' . $src->height );

        $image->addSize( $size );
        $this->getBaseController()->persist( $size );
        $this->log( 'Image size being created: ' . $size->getName() );

        $dst               = new \stdClass();
        $dst->folder       = $this->getImageDatedFolder( $image, $size );
        $dst->file         = $dst->folder . DIRECTORY_SEPARATOR . $image->getFilename();
        $dst->image        = $image;
        $dst->size         = $size;
        $dst->maxWidth     = $size->getMaxWidth();
        $dst->maxHeight    = $size->getMaxHeight();
        $dst->width        = $dst->maxWidth;
        $dst->height       = intval( $dst->width * $image->getRatio() );
        $dst->crop         = $size->getCrop();
        $dst->cropLocation = $size->getCropLocation();
        $dst->ratio        = $image->getRatio();
        $dst->ratio2       = $image->getRatioReverse();

        $this->log( 'DST Folder: ' . $dst->folder );
        $this->log( 'DST File: ' . $dst->file );
        $this->log( 'DST Width: ' . $dst->width );
        $this->log( 'DST Height: ' . $dst->height );
        $this->log( 'DST Ratio: ' . $dst->ratio );
        $this->log( 'DST Ratio2: ' . $dst->ratio2 );

        if ( $size->getCrop() ) {
            $dst->width  = $size->getMaxWidth();
            $dst->height = $size->getMaxHeight();
            $this->log( 'DST ALT Width: ' . $dst->width );
            $this->log( 'DST ALT Height: ' . $dst->height );
        } else if ( intval( $size->getMaxHeight() ) > 0 ) {
            if ( $dst->height > $dst->maxHeight ) {
                $dst->height = $dst->maxHeight;
                if ( !$dst->crop ) {
                    $dst->width = intval( $dst->height * $image->getRatioReverse() );
                }
                $this->log( 'DST ALT Width: ' . $dst->width );
                $this->log( 'DST ALT Height: ' . $dst->height );
            }
        }

        // are we forcing a format?
        $forcedFormat = $size->getForceFormat();
        if ( !empty( $forcedFormat ) ) {
            $ext       = $image->getExtension();
            $dst->file = str_replace( $ext, $forcedFormat, $dst->file );
            $this->log( 'Forced File: ' . $dst->file );
        }

        $this->resizeAndMoveUploadedImage( $src, $dst );

//        echo '</fieldset>';
    }

    public function showImage( Image &$image = NULL, ImageSize $size = NULL, $count = TRUE )
    {
        $data         = $this->getImageData( $image, $size );
        $forcedFormat = NULL;
        if ( !empty( $size ) ) {
            $forcedFormat = $size->getForceFormat();
        }
        $mediaType = $image->getMimeType();
        if ( !empty( $forcedFormat ) ) {
            switch ( $forcedFormat ) {
                case 'jpg':
                    $mediaType = 'image/jpeg';
                    break;
                default:
                    $mediaType = 'image/' . $forcedFormat;
            }
        }
        header( 'Content-Type: ' . $mediaType );
        if ( empty( $data ) ) {
            http_response_code( 404 );
        } else {
            if ( $count ) {
//                $image->incViews();
//                $this->persistAndFlush( $image );
            }
            echo $data;
        }
        exit;
    }

    public function getImageData( Image &$image = NULL, ImageSize $size = NULL )
    {
        $file = $this->getImageDatedFolder( $image, $size )
                . DIRECTORY_SEPARATOR . $image->getFilename();

        if ( !empty( $size ) ) {
            $forcedType = $size->getForceFormat();
            if ( !empty( $forcedType ) ) {
                $file = str_replace( '.' . $image->getExtension(), '.' . $forcedType, $file );
            }
        }

        if ( is_file( $file ) ) {
            return file_get_contents( $file );
        } else {
            if ( $size->hasFallback() ) {
                $fallback = $size->getFallback();

                return $this->getImageData( $image, $fallback );
            }
        }

        return NULL;
    }

    /**
     * @param Image          $image
     * @param ImageSize|NULL $size
     * @param bool           $public
     *
     * @return mixed|null
     */
    public function getImageDatedFolder( Image &$image, ImageSize $size = NULL, $public = FALSE )
    {
        return $this->getDatedImageFolder( $image->getCreatedAt(), $size, !!$public );
    }

    /**
     * @param \DateTime      $date
     * @param ImageSize|NULL $size
     * @param bool           $public
     *
     * @return mixed|null
     */
    public function getDatedImageFolder( \DateTime $date, Imagesize $size = NULL, $public = FALSE )
    {
        $subFolders = explode( '::', $date->format( 'Y::m::d::H' ) );
        $folder     = $this->getFolder( !!$public ? 'public.images' : 'images' );

        foreach ( $subFolders as $subFolder ) {
            $folder = $this->getBaseController()->verifyFolder( $folder . DIRECTORY_SEPARATOR . $subFolder );
        }

        if ( $size instanceof ImageSize && $size !== NULL ) {
            $slug   = $size->getSlug();
            $folder = $this->getBaseController()->verifyFolder( $folder . DIRECTORY_SEPARATOR . $slug );
        }

        return $folder;
    }

    /**
     * @return string
     */
    public function getFilenamePrefix()
    {
        $user   = $this->getUser();
        $prefix = 0 . rand( 0, 9 ) . rand( 0, 9 ) . rand( 0, 9 );
        if ( !empty( $user ) ) {
            try {
                $userid = $user->getId();
                if ( !empty( $userid ) ) {
                    $prefix = $userid;
                }
            } catch ( \Exception $e ) {
                // do nothing
            }
        }

        return $prefix;
    }

    public function displayTemporaryImage( $src, $dst )
    {
//        /*** REMOVE START ****/
//        $tmpFileExt  = pathinfo( $dst->file, PATHINFO_EXTENSION );
//        $tmpFilename = pathinfo( $dst->file, PATHINFO_FILENAME ) . '.' . $tmpFileExt;
//        $this->log( 'Temp Filename: ' . $tmpFilename );
//        $tmpFile = $this->getFolder( 'public.temp' ) . DIRECTORY_SEPARATOR . $dst->size->getSlug() . '.' . $tmpFilename;
//        $this->log( 'Temp Filename: ' . $tmpFile );
//        copy( $dst->file, $tmpFile );
//        $web = $this->getBaseController()->getWebFolder();
//        $this->log( 'Web Folder: ' . $web );
//        $srcFile = str_replace( $web, '', $tmpFile );
//        $this->log( 'Src File: ' . $srcFile );
//        if ( is_file( $tmpFile ) ) {
//            $this->log( '<img src="' . $srcFile . '" />' );
//        }
//        /**** REMOVE END ****/
    }

    protected function resizeAndMoveUploadedImage( &$src, &$dst )
    {
        if ( !!$dst->crop ) {
            return $this->resizeCropAndMoveUploadedImage( $src, $dst );
        }

//        echo '<fieldset><legend>Resize and Move Uploaded Image</legend>';

        $isGif    = $src->image->getExtension() === 'gif';
        $animated = !!$dst->size->getAnimate();
        $this->log( 'Animated: (' . $src->image->getExtension() . ') ' . ( $animated ? 'YES' : 'NO' ) );
        if ( $isGif && $animated ) {
            if ( GifFrameExtractor::isAnimatedGif( $src->file ) ) {
                $this->log( '<span style="color:red">THIS IS AN ANIMATED GIF</span>' );

                return $this->resizeAndMoveUploadedGifImage( $src, $dst );
            }
        }

        $imagine = new Imagine();
        $size    = new Box( $dst->width, $dst->height );
        $imagine->open( $src->file )->resize( $size )->save( $dst->file );

        $this->displayTemporaryImage( $src, $dst );

//        echo '</fieldset>';

        $this->updatePreviousSource( $src, $dst );

        return TRUE;
    }

    protected function resizeAndMoveUploadedGifImage( &$src, &$dst )
    {
//        echo '<fieldset><legend>Resize and move Uploaded GIF Image</legend>';
        // if the sizes are as we need them, just copy and return true
        if ( $src->width === $dst->width && $src->height === $dst->height ) {
            @copy( $src->file, $dst->file );

            return TRUE;
        }

        $gfe             = new GifFrameExtractor();
        $frames          = $gfe->extract( $src->file );
        $retouchedFrames = array();
        foreach ( $frames as $frame ) {
            $layer = ImageWorkshop::initFromResourceVar( $frame[ 'image' ] );
            $layer->resizeInPixel( $dst->width, NULL, TRUE );

            if ( $dst->crop ) {
                $cWidth  = $dst->width;
                $cHeight = intval( $cWidth * $src->image->getRatio() );
                $offsetX = $this->determineOffsetX( $dst->cropLocation, $cWidth, $dst->width );
                $offsetY = $this->determineOffsetY( $dst->cropLocation, $cHeight, $dst->height );
                $layer->crop( ImageWorkshopLayer::UNIT_PIXEL, $dst->width, $dst->height, $offsetX, $offsetY );
            }
            $im                = $layer->getResult( 'FF0000' );
            $retouchedFrames[] = $im;
        }

        $gc = new GifCreator();
        $gc->create( $retouchedFrames, $gfe->getFrameDurations(), 0 );
        file_put_contents( $dst->file, $gc->getGif() );

        $this->displayTemporaryImage( $src, $dst );

//        echo '</fieldset>';

        return TRUE;
    }

    protected function resizeCropAndMoveUploadedImage( &$src, &$dst )
    {
        if ( !$dst->crop ) {
            return $this->resizeAndMoveUploadImage( $src, $dst );
        }

        $isGif    = $src->image->getExtension() === 'gif';
        $animated = !!$dst->size->getAnimate();
        $this->log( 'Animated: (' . $src->image->getExtension() . ') ' . ( $animated ? 'YES' : 'NO' ) );
        if ( $isGif && $animated ) {
            if ( GifFrameExtractor::isAnimatedGif( $src->file ) ) {
                $this->log( '<span style="color:red">THIS IS AN ANIMATED GIF</span>' );

                return $this->resizeAndMoveUploadedGifImage( $src, $dst );
            }
        }

//        echo '<fieldset><legend>Resize, Crop and Move Uploaded Image</legend>';
        $old    = $src;
        $width  = $src->width;
        $height = $src->height;

        $this->log( $width . 'x' . $height );

        // we really should make the image as small as possible here
        $continue = TRUE;
        while ( $continue ) {
            if ( $width > $dst->width + 5 && $height > $dst->height + 5 ) {
                if ( $width > $dst->width ) {
                    $tWidth  = $width - 1;
                    $tHeight = intval( $tWidth * $src->image->getRatio() );
                    if ( $tWidth > $dst->width + 5 && $tHeight > $dst->height + 5 ) {
                        $width  = $tWidth;
                        $height = $tHeight;
                    } else {
                        $continue = FALSE;
                    }
                }
            } else {
                $continue = FALSE;
            }
        }

        $this->log( 'Width: ' . $width );
        $this->log( 'Height: ' . $height );

        $imagine = new Imagine();
        $size    = new Box( $width, $height );
        $image   = $imagine->open( $src->file );
        $image->resize( $size );

        $this->log( 'Cropping image: ' . $dst->cropLocation );
        $offsetX = $this->determineOffsetX( $dst->cropLocation, $width, $dst->width );
        $offsetY = $this->determineOffsetY( $dst->cropLocation, $height, $dst->height );
        $this->log( 'Offset X: ' . $offsetX );
        $this->log( 'Offset Y: ' . $offsetY );

        $point = new Point( $offsetX, $offsetY );
        $size  = new Box( $dst->width, $dst->height );
        $image->crop( $point, $size )->save( $dst->file );

        $this->updatePreviousSource( $src, $dst );
        $this->displayTemporaryImage( $src, $dst );

//        echo '</fieldset>';

        return TRUE;
    }

    protected function determineOffsetY( $cropLocation, $height, $dstHeight )
    {
//        echo '<fieldset><legend>Determing Offset Y ' . $height . ' : ' . $dstHeight . '</legend>';
        $offset       = 0;
        $cropLocation = preg_replace( '/[^a-z\s]+/', '', strtolower( trim( str_replace( '-', ' ', $cropLocation ) ) ) );
        $cropLocation = explode( ' ', $cropLocation );
        $cropLocation = $cropLocation[ 0 ];

        $this->log( 'CropLocation: ' . $cropLocation );
        if ( stristr( $cropLocation, 'bottom' ) ) {
            $this->log( 'Crop Bottom' );
            $offset = intval( $height ) - intval( $dstHeight );
        } else if ( stristr( $cropLocation, 'center' ) ) {
            $this->log( 'Crop Center' );
            $offset = ( intval( $height ) - intval( $dstHeight ) ) / 2;
        }

//        echo '</fieldset>';

        return floor( $offset );
    }

    protected function determineOffsetX( $cropLocation, $width, $dstWidth )
    {
//        echo '<fieldset><legend>Determing Offset X ' . $width . ' : ' . $dstWidth . '</legend>';
        $cropLocation = preg_replace( '/[^a-z\s]+/', '', strtolower( trim( str_replace( '-', ' ', $cropLocation ) ) ) );
        $cropLocation = explode( ' ', $cropLocation );
        $cropLocation = $cropLocation[ 1 ];

        $this->log( 'CropLocation: ' . $cropLocation );
        $offset = 0;
        if ( stristr( $cropLocation, 'right' ) ) {
            $this->log( 'Crop Right' );
            $offset = intval( $width ) - intval( $dstWidth );
        } else if ( stristr( $cropLocation, 'center' ) ) {
            $this->log( 'Crop Center' );
            $offset = ( intval( $width ) - intval( $dstWidth ) ) / 2;
        }

//        echo '</fieldset>';

        return floor( $offset >= 0 ? $offset : 0 );
    }

    /**
     * Initialize the array of usable folders
     */
    protected function initializeFolders()
    {
        $this->folders = array(
            'media'        => $this->getBaseController()->getMediaFolder(),
            'publicmedia'  => $this->getBaseController()->getPublicMediaFolder(),
            'images'       => $this->getBaseController()->getMediaImagesFolder(),
            'publicimages' => $this->getBaseController()->getPublicMediaImagesFolder(),
            'publictemp'   => $this->getBaseController()->getPublicMediaTempFolder(),
            'temp'         => $this->getBaseController()->getMediaTempFolder(),
            'publicupload' => $this->getBaseController()->getPublicMediaUploadFolder(),
            'upload'       => $this->getBaseController()->getMediaUploadFolder(),
            'publicchunks' => $this->getBaseController()->getPublicMediaChunksFolder(),
            'chunks'       => $this->getBaseController()->getMediaChunksFolder()
        );
    }

    /**
     * @param null $which
     *
     * @return mixed|null
     */
    protected function getFolder( $which = NULL )
    {
        $which = preg_replace( '/[^a-z]+/', '', strtolower( trim( $which ) ) );

        return isset( $this->folders[ $which ] ) ? $this->folders[ $which ] : NULL;
    }

    /**
     * @param int $seconds
     */
    protected function cleanupImages( $seconds = 600 )
    {
//        echo '<fieldset><legend>Cleanup Image Folders</legend>';
        $folders = array(
            'temp', 'upload', 'chunks', 'public.temp', 'public.upload', 'public.chunks'
        );
        foreach ( $folders as $folder ) {
            $folder = $this->getFolder( $folder );
//            echo 'Cleanup Folder: ' . $folder . '<br>';
            $this->cleanupFolder( $folder );
        }
//        echo '</fieldset>';
    }
}

/**
 * Class ImageManager2
 * @package CoreSys\CoreBundle\Manager
 */
class ImageManager2 extends BaseManager
{

    /**
     * @var
     */
    private $repo;

    /**
     * @var
     */
    private $image_size_manager;

    /**
     * ImageManager constructor.
     *
     * @param BaseController|NULL     $baseController
     * @param NULL|ContainerInterface $container
     */
    public function __construct( $baseController, $container )
    {
        parent::__construct( $baseController, $container );
        $this->debug = TRUE;
        $this->cleanupImages();
    }

    /**
     * @return ImageSizeManager
     */
    public function getImageSizeManager()
    {
        if ( !empty( $this->image_size_manager ) ) {
            return $this->image_size_manager;
        }

        return $this->image_size_manager = $this->get( 'core_sys_core.manager.image_size' );
    }

    /**
     * @param UploadedFile $file
     * @param bool         $save
     *
     * @return bool|Image|string
     */
    public function processUploadedFile( UploadedFile $file, $save = TRUE )
    {
        if ( !$file instanceof UploadedFile ) {
            return 'File is not an instance of UploadedFile';
        }

        $image = new Image();
        $image->setData( $file );

        $image = $this->processNewImage( $file, $image, !!$save );
        $this->cleanupImages();

        return $image;
    }

    /**
     * @param UploadedFile $file
     * @param Image        $image
     * @param bool         $save
     * @param bool         $createBase64
     *
     * @return Image|string
     */
    public function processUploadedImage( UploadedFile $file, Image &$image, $save = TRUE, $createBase64 = FALSE )
    {
        if ( !$file instanceof UploadedFile ) {
            return 'File is not an instance of UploadedFile';
        }

        $this->log( '<h1>Process Upload Image</h1>' );

        $image->setData( $file );
        $this->processNewImage( $file, $image, !!$save, !!$createBase64 );

        return $image;
    }

    /**
     * @param UploadedFile $file
     * @param Image        $image
     * @param bool         $save
     * @param bool         $createBase64
     *
     * @return bool|Image
     */
    public function processNewImage( UploadedFile &$file, Image &$image, $save = TRUE, $createBase64 = FALSE )
    {
        $uploadFolder   = $this->getBaseController()->getPublicMediaUploadFolder();
        $uploadFilename = $image->getFilename();
        $uploadFile     = $uploadFolder . DIRECTORY_SEPARATOR . $uploadFilename;

        $this->log( 'Upload Folder: ' . $uploadFolder );
        $this->log( 'Upload Filename: ' . $uploadFilename );
        $this->log( 'Upload File: ' . $uploadFile );

        $ext      = pathinfo( $uploadFilename, PATHINFO_EXTENSION );
        $ext      = $ext === 'jpeg' ? 'jpg' : $ext;
        $filename = microtime( TRUE ) . '.' . $ext;
        $image->setFilename( $filename );
        $this->log( 'New Filename: ' . $filename );

        if ( !is_file( $uploadFile ) ) {
            $file->move( $uploadFolder, $uploadFile );
            if ( !is_file( $uploadFile ) ) {
                return 'Could not upload image';
            }
        }

        $dimensions = getimagesize( $uploadFile );
        $width      = $height = 0;
        list( $width, $height ) = $dimensions;
        $this->log( 'Dimensions: ' . $width . 'x' . $height );
        $image->setWidth( $width );
        $image->setHeight( $height );

        $ratio1 = $width / $height;
        $ratio2 = $height / $width;
        $image->setRatio( $ratio1 );
        $image->setRatioReverse( $ratio2 );
        $this->log( 'Ratio : ' . $ratio1 );
        $this->log( 'Ratio2: ' . $ratio2 );

        $filesize = filesize( $uploadFile );
        $this->log( 'Filesize: ' . $filesize );
        $image->setFilesize( $filesize );

        $result = $this->moveUploadedFileToDatedFolder( $image, $uploadFile );
        if ( $result !== TRUE ) {
            $this->log( 'Result: ' . $result );

            return $result;
        }

        $this->log( 'Ready to create image sizes' );
        $this->log( '-----------------------------' );
        $this->createImageSizes( $image );

//        unlink( $uploadFile );
//
//        if( $save === true ) {
//            $this->persist( $image );
//            $this->flush();
//        }
//
//        $this->cleanupImages();
//        exit;

        return $image;
    }

    /**
     * @param Image $image
     *
     * @return bool
     */
    public function createImageSizes( Image &$image )
    {
        $this->log( '<h3>Create Image Sizes</h3>' );
        $src   = $this->getDatedImageFolder( $image->getCreatedAt() )
                 . DIRECTORY_SEPARATOR . $image->getFilename();
        $sizes = $this->getImageSizeManager()->getAvailableImageSizes( TRUE, TRUE );

        $this->log( count( $sizes ) . ' Sizes' );

        foreach ( $sizes as $size ) {
            $this->createImageSize( $image, $size, $src );
        }

        return TRUE;
    }

    /**
     * @param Image     $image
     * @param ImageSize $size
     */
    public function createImageSize( Image &$image, Imagesize &$size )
    {
        $this->log( 'Attempting to create image size: ' . $size->getName() );
        $src         = new \stdClass();
        $src->folder = $this->getDatedImageFolder( $image->getCreatedAt() );
        $src->file   = $src->folder . DIRECTORY_SEPARATOR . $image->getFilename();
        $src->size   = getimagesize( $src->file );
        $src->width  = $src->size[ 0 ];
        $src->height = $src->size[ 1 ];
        $src->ratio  = $src->height / $src->width;
        $src->ratio2 = $src->width / $src->height;
        $src->image  = $image;
        $src->ext    = $image->getExtension();

        $image->setWidth( $src->width );
        $image->setHeight( $src->height );
        $image->setRatio( $src->ratio );
        $image->setRatioReverse( $src->ratio2 );
        $image->setFilesize( filesize( $src->file ) );

        if ( intval( $size->getMaxWidth() ) > $src->width ) {
            $image->removeSize( $size );
            $this->log( '<span style="color:red">Size is too large(' . $size->getMaxWidth() . 'x' . $src->width . ')</span>' );

            return FALSE;
        } else {
            $image->addSize( $size );
            $this->getBaseController()->persist( $size );
        }
        $this->log( '<span style="color:green">Size being created ' . $size->getName() . '(' . $size->getMaxWidth() . 'x' . $size->getMaxHeight() . ')</span>' );

        $cropLocation = $size->getCropLocation();

        $dst               = new \stdClass();
        $dst->folder       = $this->getDatedImageFolder( $image->getCreatedAt(), $size );
        $dst->file         = $dst->folder . DIRECTORY_SEPARATOR . $image->getFilename();
        $dst->maxWidth     = $size->getMaxWidth();
        $dst->maxHeight    = $size->getMaxHeight();
        $dst->width        = $dst->maxWidth;
        $dst->height       = intval( $dst->width * $image->getRatio() );
        $dst->crop         = !!$size->getCrop();
        $dst->cropLocation = str_replace( '-', ' ', !empty( $cropLocation ) ? $cropLocation : 'center center' );

        // check the width and height
        if ( intval( $dst->maxHeight ) > 0 ) {
            if ( $dst->height > $dst->maxHeight ) {
                $dst->height = $dst->maxHeight;

                if ( !$dst->crop ) {
                    $dst->width = intval( $dst->height * $image->getRatioReverse() );
                }
            }
        }

        $this->resizeAndMoveUploadedImage( $src, $dst );
    }

    /**
     * @param $src
     * @param $dst
     *
     * @return bool
     */
    public function resizeAndMoveUploadedImage( &$src, &$dst )
    {
        if ( $dst->crop === TRUE ) {
            return $this->resizeCropAndMoveUploadedImage( $src, $dst );
        }

        $this->log( '<h2>Resize and Move</h2>' );
    }

    /**
     * @param $src
     * @param $dst
     *
     * @return bool
     */
    public function resizeCropAndMoveUploadedImage( &$src, &$dst )
    {
        if ( $dst->crop === FALSE ) {
            return $this->resizeAndMoveUploadedImage( $src, $dst );
        }

        $this->log( '<h2>Resize, Crop and Move</h2>' );

        $this->log( 'Cropping: ' . $dst->width . 'x' . $dst->height );

        $old = $src;
        if ( $src->width > $src->height ) {
            $height = $dst->height;
            $width  = intval( $height * ( $src->width / $src->height ) );
        } else {
            $width  = $dst->width;
            $height = intval( $width * $src->ratio );
        }

        $this->log( 'Resize image to: ' . $width . 'x' . $height );

        $imagine = new Imagine();
        $size    = new Box( $width, $height );
        $image   = $imagine->open( $src->file );
        $image->resize( $size );

        $this->log( 'Cropping Image: ' . $dst->cropLocation );
        $offset_x = $this->determineOffsetX( $dst->cropLocation, $width, $dst->width );
        $offset_y = $this->determineOffsetY( $dst->cropLocation, $height, $dst->height );
        $this->log( 'Offsets: (' . $dst->cropLocation . ') ' . $offset_x . 'x' . $offset_y );

        $this->log( 'Saving dst file: ' . $dst->file );
        $point = new Point( $offset_x, $offset_y );
        $size  = new Box( $dst->width, $dst->height );
        $image->crop( $point, $size )->save( $dst->file );

        // @todo remove later
        $tmpFilename = pathinfo( $dst->file, PATHINFO_FILENAME );
        $tmpExt      = pathinfo( $dst->file, PATHINFO_EXTENSION );
        $tmpFolder   = $this->getBaseController()->getMediaTempFolder();
        $tmpFile     = $this->getBaseController()->getPublicMediaTempFolder()
                       . DIRECTORY_SEPARATOR . $tmpFilename . $tmpExt;
        $webFolder   = $this->getBaseController()->getWebFolder();
        $tmpFilename = str_replace( $webFolder, '', $tmpFile );

        copy( $dst->file, $tmpFile );

        $this->log( 'Temp File: ' . $tmpFile );
        $this->log( 'Temp Filename: ' . $tmpFilename );
        $this->log( '<img src="' . $tmpFilename . '" />' );

        $src = $old;

        return TRUE;
    }

    /**
     * @param Image $image
     * @param       $uploadFile
     *
     * @return bool
     */
    public function moveUploadedFileToDatedFolder( Image &$image, $uploadFile )
    {
        $dstFolder = $this->getDatedImageFolder( $image->getCreatedAt() );
        $dstFile   = $dstFolder . DIRECTORY_SEPARATOR . $image->getFilename();

        try {
            copy( $uploadFile, $dstFile );

            return TRUE;
        } catch ( \Exception $e ) {
            return FALSE;
        }
    }

    /**
     * @param \DateTime      $date
     * @param ImageSize|NULL $size
     * @param bool           $public
     *
     * @return mixed
     */
    public function getDatedImageFolder( \DateTime $date, ImageSize $size = NULL, $public = FALSE )
    {
        $subFolders = explode( '::', $date->format( 'Y::m::d::H' ) );
        $folder     = $public ? $this->getBaseController()->getPublicMediaImagesFolder()
            : $this->getBaseController()->getMediaImagesFolder();

        foreach ( $subFolders as $subFolder ) {
            $folder = $this->getBaseController()->verifyFolder( $folder . DIRECTORY_SEPARATOR . $subFolder );
        }

        if ( $size instanceof ImageSize && $size !== NULL ) {
            $slug   = $size->getSlug();
            $folder = $this->verifyFolder( $folder . DIRECTORY_SEPARATOR . $slug );
        }

        return $folder;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepo()
    {
        if ( !empty( $this->repo ) ) {
            return $this->repo;
        }

        return $this->repo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Image' );
    }

    /**
     * @return object
     */
    public function getSizeManager()
    {
        if ( !empty( $this->image_size_manager ) ) {
            return $this->image_size_manager;
        }

        return $this->image_size_manager = $this->getBaseController()->get( 'core_sys_core.manager.image_size' );
    }

    /**
     * Cleanup all temp/upload folders
     */
    public function cleanupImages()
    {
        $this->cleanupTempFolder( TRUE );
        $this->cleanupTempFolder( FALSE );
        $this->cleanupUploadsFolder( TRUE );
        $this->cleanupUploadsFolder( FALSE );
        $this->cleanupChunksFolder( TRUE );
        $this->cleanupChunksFolder( FALSE );
    }

    /**
     * @param bool $public
     *
     * @return $this
     */
    public function cleanupChunksFolder( $public = FALSE )
    {
        $folder = $public ? $this->getBaseController()->getPublicMediaChunkFolder()
            : $this->getBaseController()->getMediaChunkFolder();

        $this->clearFolderFiles( $folder, 600 );

        return $this;
    }

    /**
     *
     */
    public function uploadChunk()
    {
        $uploadFolder = $this->getPublicMediaUploadFolder();
        $chunksFolder = $this->getPublicMediaChunksFolder();

        $config = new FlowConfig();
        $config->setTempDir( $chunksFolder );

        $request  = new FlowRequest();
        $filename = $request->getFilename();

        $ext = pathinfo( $filename, PATHINFO_EXTENSION );
        $ext = $ext === 'jpeg' ? 'jpg' : $ext;

        $filename = str_replace( '.', '_', microtime( TRUE ) ) . '.' . $ext;
        $dstFile  = $uploadFolder . DIRECTORY_SEPARATOR . $filename;

        if ( FlowBasic::save( $dstFile, $config, $request ) ) {
            if ( is_file( $dstFile ) ) {
                $image = $this->processUploadedFile( $dstFile, TRUE, FALSE );
                $data  = array(
                    'complete' => TRUE,
                    'filename' => $filename,
                    'image'    => $image
                );
                header( 'Content-Type: application/json' );
                echo $this->serializeData( $data, 'json', TRUE, TRUE );
                exit;
            }
        } else {
            $data = array(
                'filename' => $filename,
                'complete' => FALSE
            );
            $this->echoJsonResponse( $data, TRUE );
        }
    }

    /**
     * @param $location
     * @param $srcw
     * @param $dstw
     *
     * @return int
     */
    public function determineOffsetX( $location, $srcw, $dstw )
    {
        if ( $dstw >= $srcw ) {
            $dstw = $srcw;
        }

        $location      = str_replace( '-', ' ', strtolower( trim( $location ) ) );
        $locationParts = explode( ' ', $location );
        $horizontal    = $locationParts[ 1 ];
        $offset        = 0;

        switch ( $horizontal ) {
            case 'right':
                $offset = intval( $srcw ) - intval( $dstw );
                break;
            case 'center':
                $offset = ( intval( $srcw ) - intval( $dstw ) ) / 2;
                break;
        }

        return intval( $offset >= 0 ? $offset : 0 );
    }

    /**
     * @param $location
     * @param $srch
     * @param $dsth
     *
     * @return int
     */
    public function determineOffsetY( $location, $srch, $dsth )
    {
        if ( $dsth >= $srch ) {
            $dsth = $srch;
        }

        $location      = str_replace( '-', ' ', strtolower( trim( $location ) ) );
        $locationParts = explode( ' ', $location );
        $horizontal    = $locationParts[ 0 ];
        $offset        = 0;

        switch ( $horizontal ) {
            case 'bottom':
                $offset = intval( $srch ) - intval( $dsth );
                break;
            case 'center':
                $offset = ( intval( $srch ) - intval( $dsth ) ) / 2;
                break;
        }

        return intval( $offset >= 0 ? $offset : 0 );
    }

    /**
     * @param bool $public
     *
     * @return $this
     */
    protected function cleanupUploadsFolder( $public = FALSE )
    {
        $folder = $public ? $this->getBaseController()->getPublicMediaUploadFolder()
            : $this->getBaseController()->getMediaUploadFolder();

        $this->clearFolderFiles( $folder, 600 );

        return $this;
    }

    /**
     * @param bool $public
     *
     * @return $this
     */
    protected function cleanupTempFolder( $public = FALSE )
    {
        $folder = $public ? $this->getBaseController()->getPublicMediaTempFolder()
            : $this->getBaseController()->getMediaTempFolder();

        $this->clearFolderFiles( $folder, 600 );

        return $this;
    }

    /**
     * @param null $folder
     * @param int  $checkTime
     */
    protected function clearFolderFiles( $folder = NULL, $checkTime = 600 )
    {
        if ( is_dir( $folder ) ) {
            $dir = opendir( $folder );
            if ( $dir ) {
                while ( FALSE !== ( $file = readdir( $dir ) ) ) {
                    if ( $file !== '.' && $file !== '..' ) {
                        $filename = $folder . DIRECTORY_SEPARATOR . $file;
                        if ( is_file( $filename ) ) {
                            $mtime = filemtime( $filename );
                            if ( $mtime < $checkTime ) {
                                @unlink( $filename );
                            }
                        }
                    }
                }
            }
        }
    }
}