<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use CoreSys\CoreBundle\Entity\Role;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class RoleManager
 * @package CoreSys\CoreBundle\Manager
 */
class RoleManager extends ConfigManager
{

    /**
     * @var string
     */
    protected $file;

    /**
     * RoleManager constructor.
     *
     * @param BaseController     $baseController
     * @param ContainerInterface $container
     */
    public function __construct( BaseController $baseController, ContainerInterface $container )
    {
        parent::__construct( $baseController, $container );
        $this->file = $this->getConfigurationFile( 'roles.yml' );
        $this->setup();
    }

    /**
     *
     */
    public function setup()
    {
        $repo  = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Role' );
        $roles = array(
            'Guest'       => NULL,
            'User'        => NULL,
            'Member'      => NULL,
            'Moderator'   => NULL,
            'Admin'       => NULL,
            'Super Admin' => NULL,
            'Owner'       => NULL
        );

        $previous = NULL;
        foreach ( $roles as $roleName => $role ) {
            $role = $repo->findOneBy( array( 'name' => $roleName ) );
            if ( !$role instanceof Role ) {
                $role = new Role();
                $role->setMandatory( TRUE )
                     ->setName( $roleName );

                if ( $previous instanceof Role ) {
                    $role->setParent( $previous );
                }

                $this->getBaseController()->persistAndFlush( $role );
                $previous = $role;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getAllRoles()
    {
        return $this->getRepo( 'CoreSysCoreBundle:Role' )->findAll();
    }

    /**
     * @return array
     */
    public function getRoleNamePairs()
    {
        $pairs = array();
        foreach ( $this->getAllRoles() as $role ) {
            $pairs[ $role->getId() ] = $role->getName();
        }

        return $pairs;
    }

    /**
     *
     */
    public function updateRoles()
    {
        $this->dumpYml();
    }

    /**
     * @param bool $return_yml
     *
     * @return array|null
     */
    public function getDataStructure( $return_yml = FALSE )
    {
        $hier = array();

        foreach ( $this->getAllRoles() as $role ) {
            $rhier  = array();
            $parent = $role->getParent();
            if ( !empty( $parent ) ) {
                $rhier[] = $parent->getRoleName();
            } else {
                $rhier[] = 'ROLE_USER';
            }
            if ( $role->getSwitch() ) {
                $rhier[] = 'ROLE_ALLOWED_TO_SWITCH';
            }
            if ( $role->getActive() ) {
                $hier[ $role->getRoleName() ] = array_unique( $rhier );
            }
        }

        if ( count( $hier ) === 0 ) {
            return NULL;
        }

        return $return_yml ? Yaml::dump( $hier, 1 ) : $hier;
    }

    /**
     * @param Role $role
     *
     * @return $this
     */
    public function deleteRole( Role $role )
    {
        foreach ( $role->getUsers() as $user ) {
            $user->removeSysRole( $role );
            $this->persist( $user );
        }
        $this->flush();

        return $this;
    }

    /**
     * @param Role $role
     *
     * @return RoleManager
     */
    public function removeRole( Role $role )
    {
        return $this->deleteRole( $role );
    }
}