<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Yaml\Yaml;


/**
 * Class ConfigManager
 * @package CoreSys\CoreBundle\Manager
 */
class ConfigManager extends BaseManager
{

    /**
     * @var
     */
    protected $file;

    /**
     * ConfigManager constructor.
     *
     * @param BaseController     $baseController
     * @param ContainerInterface $container
     */
    public function __construct( BaseController $baseController, ContainerInterface $container )
    {
        parent::__construct( $baseController, $container );
    }

    /**
     * @param null $yml
     */
    function writeYmlToFile( $yml = NULL )
    {
        if ( $yml === NULL ) {
            $fp = fopen( $this->file, 'w+' );
            if ( $fp ) {
                fclose( $fp );
            }

            return;
        }
        $fs = new Filesystem();
        $fs->dumpFile( $this->file, $yml, 0777 );
    }

    /**
     *
     */
    function dumpYml()
    {
        $yml = $this->getDataStructure( TRUE );
        $this->writeYmlToFile( $yml );
    }

    /**
     * @param null $filename
     *
     * @return string
     */
    public function getConfigurationFile( $filename = NULL )
    {
        $base = dirname( __DIR__ );
        $app  = $base . DIRECTORY_SEPARATOR . 'app';
        while ( !is_dir( $app ) ) {
            $base = dirname( $base );
            $app  = $base . DIRECTORY_SEPARATOR . 'app';
        }

        return implode( DIRECTORY_SEPARATOR, array(
            $app, 'config', $filename
        ) );
    }

    public function getBaseController()
    {
        return $this->baseController;
    }

}