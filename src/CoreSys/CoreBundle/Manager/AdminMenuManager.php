<?php

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use CoreSys\CoreBundle\Entity\AdminMenu;
use Doctrine\Common\Persistence\ObjectRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;

/**
 * Class AdminMenuManager
 * @package CoreSys\CoreBundle\Manager
 * @DI\Service("core_sys_core.manager.admin_menu", parent="core_sys_core.manager.base")
 */
class AdminMenuManager extends BaseManager
{

    /**
     * @var ObjectRepository
     */
    private $adminMenusRepo;

    /**
     * @var integer
     */
    private $adminMenusCount = -1;

    /**
     * @var array
     */
    private $adminMenus = array();

    public function __construct( $baseController, $container )
    {
        parent::__construct( $baseController, $container );
        $this->debug = TRUE;
    }

    /**
     * @return ObjectRepository
     */
    public function getAdminMenusRepo()
    {
        if ( !empty( $this->adminMenusRepo ) ) {
            return $this->adminMenusRepo;
        }

        return $this->adminMenusRepo = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:AdminMenu' );
    }

    /**
     * @param bool $force
     *
     * @return int
     */
    public function getAdminMenusCount( $force = FALSE )
    {
        if ( !$force ) {
            if ( intval( $this->adminMenusCount ) > 0 ) {
                return $this->adminMenusCount;
            }
        }

        return $this->adminMenusCount = $this->getAdminMenusRepo()->getMenusCount();
    }

    /**
     * @return array
     */
    public function getAdminMenus()
    {
        if ( !empty( $this->adminMenus ) && count( $this->adminMenus ) > 0 ) {
            return $this->adminMenus;
        }

        return $this->adminMenus = $this->getAdminMenusRepo()->findAll();
    }

    /**
     *
     */
    public function checkAdminMenus()
    {
        $this->debug = FALSE;
        $this->log( 'Check Admin Menus' );
        $foundMenus = array();
        $this->checkSourceForAdminMenus( $foundMenus );
        $this->log( 'Complete Checking Source' );
        $this->validateAdminMenus( $foundMenus );
        $this->log( 'Complete Validating' );
    }

    /**
     * @param $menu
     *
     * @return bool
     */
    public function isMenuInDatatabase( $menu )
    {
        $menuObj = $this->getAdminMenusRepo()->findBy( $menu );

        return !empty( $menuObj );
    }

    /**
     * @param $menus
     */
    private function validateAdminMenus( &$menus )
    {
        $this->log( '<hr>' );
        $this->log( $menus );
        $this->log( '<hr>' );
        foreach ( $menus as $menu ) {
            $has = $this->isMenuInDatatabase( $menu );
            $this->log( 'Has Menu? ' . ( $has ? 'YES' : 'NO' ) );
            if ( !$has ) {
                $this->addMenuToDatabase( $menu );
            }
        }

        foreach ( $this->getAdminMenus() as $key => $menuObj ) {
            $has = FALSE;
            foreach ( $menus as $menu ) {
                $namespace = $menuObj->getNamespace();
                $bundle    = $menuObj->getBundle();
                if ( $menu[ 'namespace' ] === $namespace && $menu[ 'bundle' ] === $bundle ) {
                    $has = TRUE;
                    break;
                }
                if ( !$has ) {
                    unset( $this->adminMenus[ $key ] );
                    $this->remove( $menuObj );
                }
            }
        }

        $this->flush();
    }

    /**
     * @param $menu
     */
    private function addMenuToDatabase( $menu )
    {
        $this->getAdminMenusCount();
        $menuObj = new AdminMenu();
        $menuObj->setNamespace( $menu[ 'namespace' ] )
                ->setBundle( $menu[ 'bundle' ] )
                ->setFile( $menu[ 'file' ] )
                ->setPosition( $this->adminMenusCount++ );
        $this->log( 'Added Menu: ' );
        $this->log( $menu );
        $this->persistAndFlush( $menuObj );
        $this->adminMenus[] = $menuObj;
    }

    /**
     * @param $menu
     */
    private function checkSourceForAdminMenus( &$menus )
    {
        $srcFolder = $this->getBaseController()->getRootFolder()
                     . DIRECTORY_SEPARATOR . 'src';
        $this->log( 'Source Folder: ' . $srcFolder );
        if ( is_dir( $srcFolder ) ) {
            $dir = opendir( $srcFolder );
            if ( $dir ) {
                while ( FALSE !== ( $namespace = readdir( $dir ) ) ) {
                    if ( $namespace !== '.' && $namespace !== '..' ) {
                        $namespaceFolder = $srcFolder . DIRECTORY_SEPARATOR . $namespace;
                        if ( is_dir( $namespaceFolder ) ) {
                            $this->checkNamespaceForAdminMenus( $namespace, $menus );
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }

    /**
     * @param $namespace
     * @param $menus
     */
    private function checkNamespaceForAdminMenus( $namespace, &$menus )
    {
        $namespaceFolder = implode( DIRECTORY_SEPARATOR, array(
            $this->getBaseController()->getRootFolder(),
            'src',
            $namespace
        ) );
        $this->log( 'NamespaceFolder: ' . $namespaceFolder );
        if ( is_dir( $namespaceFolder ) ) {
            $dir = opendir( $namespaceFolder );
            if ( $dir ) {
                while ( FALSE !== ( $bundle = readdir( $dir ) ) ) {
                    if ( $dir !== '.' && $dir !== '..' && strstr( $bundle, 'Bundle' ) ) {
                        $this->checkBundleForAdminMenus( $namespace, $bundle, $menus );
                    }
                }
                closedir( $dir );
            }
        }
    }

    /**
     * @param $namespace
     * @param $bundle
     * @param $menus
     */
    private function checkBundleForAdminMenus( $namespace, $bundle, &$menus )
    {
        $menuFolder = implode( DIRECTORY_SEPARATOR, array(
            $this->getBaseController()->getRootFolder(),
            'src',
            $namespace,
            $bundle,
            'Resources', 'views', 'AdminMenu'
        ) );
        $this->log( 'MenuFolder: ' . $menuFolder );

        if ( is_dir( $menuFolder ) ) {
            $dir = opendir( $menuFolder );
            if ( $dir ) {
                while ( FALSE !== ( $file = readdir( $dir ) ) ) {
                    if ( $file != '.' && $file !== '..' ) {
                        $filename = $menuFolder . DIRECTORY_SEPARATOR . $file;
                        if ( is_file( $filename ) ) {
                            $this->log( 'Found Menu: ' . $file );
                            $menus[] = array(
                                'namespace' => $namespace,
                                'bundle'    => $bundle,
                                'file'      => $file
                            );
                        }
                    }
                }
                closedir( $dir );
            }
        }
    }
}