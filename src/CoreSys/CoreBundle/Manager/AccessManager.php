<?php
/**
 * Created by PhpStorm.
 * User: jmccreig
 * Date: 6/14/2016
 * Time: 8:44 PM
 */

namespace CoreSys\CoreBundle\Manager;

use CoreSys\CoreBundle\Controller\BaseController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AccessManager
 * @package CoreSys\CoreBundle\Manager
 */
class AccessManager extends ConfigManager
{

    /**
     * @var string
     */
    protected $file;

    public function __construct( BaseController $baseController, ContainerInterface $container )
    {
        parent::__construct( $baseController, $container );
        $this->file = $this->getConfigurationFile( 'access.yml' );
    }

    /**
     * @param bool $return_yml
     *
     * @return array|null
     */
    function getDataStructure( $return_yml = FALSE )
    {
        $return = array();
        $repo   = $this->getBaseController()->getRepo( 'CoreSysCoreBundle:Access' );
        foreach ( $repo->findAll() as $access ) {
            if ( $access->getActive() ) {
                $data     = $access->getData();
                $return[] = $data;
            }
        }

        if ( count( $return ) == 0 ) {
            return NULL;
        }

        return $return_yml ? Yaml::dump( $return, 1 ) : $return;
    }
}